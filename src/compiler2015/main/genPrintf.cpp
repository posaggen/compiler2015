#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <bitset>

typedef long long LL;
#define pb push_back
#define MPII make_pair<int, int>
#define PII pair<int, int>
#define sz(x) (int)x.size()

using namespace std;

template<class T> T abs(T x){if (x < 0) return -x; else return x;}

char c[10000];

int main(){
	freopen("printf.txt", "r", stdin);
	freopen("out_printf.txt", "w", stdout);
	while (gets(c)){
		int len = strlen(c);
		if (len == 0) continue;
		printf("System.out.println(\"%s\");\n", c);
	}
	return 0;
}

