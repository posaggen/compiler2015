
.data
msg:.asciiz "dayeshabi~\n"
.extern foobar 4

.text

printf_loop:
lb $a0, 0($a2)
beq $a0, 0, printf_end
add $a2, $a2, 1
beq $a0, '\', printf_enter
beq $a0, '%', printf_fmt
li $v0, 11
syscall
b printf_loop

printf_end:
jr $ra

printf_fmt:
lb $a0, 0($a2)
add $a2, $a2, 1
beq $a0, 'd', printf_int
beq $a0, 's', printf_str
beq $a0, 'c', printf_char
beq $a0, '.', printf_width
b printf_loop

printf_int:
lw $a0, 0($a1)
sub $a1, $a1, 4
li $v0, 1
syscall
b printf_loop

printf_str:
lw $a3, 0($a1)
sub $a1, $a1, 4
printf_str_lb1:
lb $a0, 0($a3)
add $a3, $a3, 1
beq $a0, 0, printf_str_lb2
li $v0, 11
syscall
b printf_str_lb1
printf_str_lb2:
b printf_loop

printf_char:
lb $a0, 0($a1)
sub $a1, $a1, 4
li $v0, 11
syscall
b printf_loop

printf_width:
add $a2, $a2, 2
lw $a3, 0($a1)
sub $a1, $a1, 4
li $a0, 32
li $v0, 11
bgt $a3, 999, print_width_lb1
syscall
bgt $a3, 99, print_width_lb1
syscall
bgt $a3, 9, print_width_lb1
syscall
print_width_lb1:
move $a0, $a3
li $v0, 1
syscall
b printf_loop

printf_enter:
add $a2, $a2, 1
li $a0, 10
li $v0, 11
syscall
b printf_loop

main:
la $a2, msg
jal printf_loop
jr $ra          # retrun to caller