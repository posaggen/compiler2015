/** Target: Check the struct assignment.
 * Possible optimization: Dead code elimination, common expression, strength reduction, inline function
 * REMARK: Pay attention to the addressing of the struct. The struct assignment should be supported.
 *
**/

#include <stdio.h>
int size1=5,size2=5;
struct node
{
    int a[5][5];
    char ch[2];
    int count;
    struct inside{int p;} in;
} a[5];

int main()
{
	printf("%d\n", '\n');
    int q=7,k,i,j;
    for (k=0;k<size2;++k)
        for (i=0;i<size1;++i)
            for (j=0;j<size1;++j)
            {
                a[k].a[i][j]=(i*5110+j)%(34-k)+1;
				a[k].ch[0]=k+i*i*i;
				a[k].ch[1]=j+i+k<<1;
				a[k].in.p=k+~i|j;
				
printf("2");
                a[k].ch[0]=a[k].ch[0]%('z'-'a'+1)+'a';
                
printf("3");
                a[k].ch[1]=a[k].ch[1]%('Z'-'A'+1)+'A';
            }

}
