.data
_size1: .space 4
__t0: .space 4
_size2: .space 4
__t1: .space 4
_a: .space 580
__t137: .space 4
__t147: .space 4
__t151: .space 4
__t275: .space 4
__t281: .space 4
__t290: .space 4
.text
_start:
la $t0, _size1
sw $t0, __t0
lw $t0, __t0
li $t1, 5
sw $t1, 0($t0)
la $t0, _size2
sw $t0, __t1
lw $t0, __t1
li $t1, 5
sw $t1, 0($t0)
jr $ra
__comp1:
sub $sp, $sp, 20
add $t0,$sp,28
sw $t0, 12($sp)
lw $t0, 12($sp)
lw $t0, 0($t0)
add $t1, $sp, 8
sw $t0, 0($t1)
add $t0,$sp,24
sw $t0, 4($sp)
lw $t0, 4($sp)
lw $t0, 0($t0)
add $t1, $sp, 0
sw $t0, 0($t1)
li $t0, 1
sw $t0, 16($sp)
lw $t0, 8($sp)
lw $t1, 0($sp)
bne $t0,$t1,___label3
li $t0, 0
sw $t0, 16($sp)
___label3:
lw $t0, 16($sp)
li $t1, 0
beq $t0,$t1,___label1
li $v0, 0
jr $ra
j ___label2
___label1:
li $v0, 1
jr $ra
___label2:
jr $ra
__compare:
sub $sp, $sp, 184
add $t0,$sp,176
sw $t0, 172($sp)
lw $t0, 172($sp)
li $t1, 0
sw $t1, 0($t0)
add $t0,$sp,180
sw $t0, 168($sp)
lw $t0, 168($sp)
li $t1, 0
sw $t1, 0($t0)
___label6:
li $t0, 1
sw $t0, 164($sp)
lw $t0, 180($sp)
lw $t1, _size1
blt $t0,$t1,___label8
li $t0, 0
sw $t0, 164($sp)
___label8:
lw $t0, 164($sp)
li $t1, 0
beq $t0,$t1,___label5
add $t0,$sp,176
sw $t0, 160($sp)
lw $t0, 160($sp)
li $t1, 0
sw $t1, 0($t0)
___label10:
li $t0, 1
sw $t0, 156($sp)
lw $t0, 176($sp)
lw $t1, _size1
blt $t0,$t1,___label12
li $t0, 0
sw $t0, 156($sp)
___label12:
lw $t0, 156($sp)
li $t1, 0
beq $t0,$t1,___label9
add $t0,$sp,304
sw $t0, 148($sp)
lw $t0, 180($sp)
li $t1, 20
mul $t0, $t0, $t1
sw $t0, 140($sp)
lw $t0, 148($sp)
lw $t1, 140($sp)
add $t0, $t0, $t1
sw $t0, 140($sp)
lw $t0, 176($sp)
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 136($sp)
lw $t0, 140($sp)
lw $t1, 136($sp)
add $t0, $t0, $t1
sw $t0, 136($sp)
lw $t0, 136($sp)
lw $t0, 0($t0)
sw $t0, 132($sp)
add $t0,$sp,188
sw $t0, 128($sp)
lw $t0, 180($sp)
li $t1, 20
mul $t0, $t0, $t1
sw $t0, 120($sp)
lw $t0, 128($sp)
lw $t1, 120($sp)
add $t0, $t0, $t1
sw $t0, 120($sp)
lw $t0, 176($sp)
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 116($sp)
lw $t0, 120($sp)
lw $t1, 116($sp)
add $t0, $t0, $t1
sw $t0, 116($sp)
lw $t0, 116($sp)
lw $t0, 0($t0)
sw $t0, 112($sp)
li $t0, 1
sw $t0, 152($sp)
lw $t0, 132($sp)
lw $t1, 112($sp)
bne $t0,$t1,___label15
li $t0, 0
sw $t0, 152($sp)
___label15:
lw $t0, 152($sp)
li $t1, 0
beq $t0,$t1,___label13
li $v0, 0
jr $ra
j ___label14
___label13:
___label14:
___label11:
lw $t0, 176($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 108($sp)
add $t0,$sp,176
sw $t0, 104($sp)
lw $t0, 104($sp)
lw $t1, 108($sp)
sw $t1, 0($t0)
j ___label10
___label9:
___label7:
lw $t0, 180($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 100($sp)
add $t0,$sp,180
sw $t0, 96($sp)
lw $t0, 96($sp)
lw $t1, 100($sp)
sw $t1, 0($t0)
j ___label6
___label5:
add $t0,$sp,304
sw $t0, 88($sp)
lw $t0, 88($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 84($sp)
li $t0, 0
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 80($sp)
lw $t0, 84($sp)
lw $t1, 80($sp)
add $t0, $t0, $t1
sw $t0, 80($sp)
lw $t0, 80($sp)
lw $t0, 0($t0)
sw $t0, 76($sp)
add $t0,$sp,188
sw $t0, 72($sp)
lw $t0, 72($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 68($sp)
li $t0, 0
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 64($sp)
lw $t0, 68($sp)
lw $t1, 64($sp)
add $t0, $t0, $t1
sw $t0, 64($sp)
lw $t0, 64($sp)
lw $t0, 0($t0)
sw $t0, 60($sp)
li $t0, 1
sw $t0, 92($sp)
lw $t0, 76($sp)
lw $t1, 60($sp)
bne $t0,$t1,___label18
li $t0, 0
sw $t0, 92($sp)
___label18:
lw $t0, 92($sp)
li $t1, 0
beq $t0,$t1,___label16
li $v0, 0
jr $ra
j ___label17
___label16:
add $t0,$sp,304
sw $t0, 52($sp)
lw $t0, 52($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 48($sp)
li $t0, 1
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 44($sp)
lw $t0, 48($sp)
lw $t1, 44($sp)
add $t0, $t0, $t1
sw $t0, 44($sp)
lw $t0, 44($sp)
lw $t0, 0($t0)
sw $t0, 40($sp)
add $t0,$sp,188
sw $t0, 36($sp)
lw $t0, 36($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 32($sp)
li $t0, 1
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 32($sp)
lw $t1, 28($sp)
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
li $t0, 1
sw $t0, 56($sp)
lw $t0, 40($sp)
lw $t1, 24($sp)
bne $t0,$t1,___label21
li $t0, 0
sw $t0, 56($sp)
___label21:
lw $t0, 56($sp)
li $t1, 0
beq $t0,$t1,___label19
li $v0, 0
jr $ra
j ___label20
___label19:
add $t0,$sp,304
sw $t0, 20($sp)
lw $t0, 20($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 16($sp)
add $t0,$sp,188
sw $t0, 12($sp)
lw $t0, 12($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 8($sp)
sub $t0, $sp,4
lw $t1, 16($sp)
lw $t2, 0($t1)
sw $t2, 0($t0)
sub $t0, $sp,8
lw $t1, 8($sp)
lw $t2, 0($t1)
sw $t2, 0($t0)
sub $sp, $sp,12
sw $ra, 0($sp)
jal __comp1
add $t1, $sp, 36
sw $v0, 0($t1)
add $sp, $sp, 20
lw $ra, 0($sp)
add $sp, $sp,12
lw $v0, 4($sp)
jr $ra
___label20:
___label17:
li $t0, 1
neg $t0, $t0
sw $t0, 0($sp)
lw $v0, 0($sp)
jr $ra
jr $ra
__getNode:
sub $sp, $sp, 52
la $t0, _a
sw $t0, 48($sp)
lw $t0, 56($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 44($sp)
lw $t0, 48($sp)
lw $t1, 44($sp)
add $t0, $t0, $t1
sw $t0, 44($sp)
lw $t0, 44($sp)
lw $t0, 108($t0)
add $t1, $sp, 40
sw $t0, 0($t1)
lw $t0, 40($sp)
sw $t0, 36($sp)
la $t0, _a
sw $t0, 32($sp)
lw $t0, 56($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 32($sp)
lw $t1, 28($sp)
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 108($t0)
add $t1, $sp, 24
sw $t0, 0($t1)
lw $t0, 24($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 20($sp)
la $t0, _a
sw $t0, 16($sp)
lw $t0, 56($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 12($sp)
lw $t0, 16($sp)
lw $t1, 12($sp)
add $t0, $t0, $t1
sw $t0, 12($sp)
lw $t0, 12($sp)
li $t1, 108
add $t0, $t0, $t1
sw $t0, 8($sp)
lw $t0, 8($sp)
lw $t1, 20($sp)
sw $t1, 0($t0)
la $t0, _a
sw $t0, 4($sp)
lw $t0, 56($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 4($sp)
lw $t1, 0($sp)
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $v0, 0($sp)
jr $ra
jr $ra
__exchange:
sub $sp, $sp, 160
add $t0,$sp,44
sw $t0, 40($sp)
lw $t0, 40($sp)
sw $t0, 32($sp)
lw $t0, 32($sp)
li $t1, 0
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 168($sp)
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 28($sp)
lw $t0, 0($t0)
sw $t0, 36($sp)
lw $t0, 32($sp)
lw $t1, 36($sp)
sw $t1, 0($t0)
lw $t0, 32($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 32($sp)
lw $t0, 28($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 28($sp)
lw $t0, 168($sp)
sw $t0, 20($sp)
lw $t0, 164($sp)
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
lw $t0, 16($sp)
lw $t0, 0($t0)
sw $t0, 24($sp)
lw $t0, 20($sp)
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $t0, 20($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 16($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 16($sp)
add $t0,$sp,44
sw $t0, 12($sp)
lw $t0, 164($sp)
sw $t0, 4($sp)
lw $t0, 12($sp)
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t0, 0($t0)
sw $t0, 8($sp)
lw $t0, 4($sp)
lw $t1, 8($sp)
sw $t1, 0($t0)
lw $t0, 4($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 4($sp)
lw $t0, 0($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 0($sp)
jr $ra
__comp:
sub $sp, $sp, 696
add $t0,$sp,580
sw $t0, 576($sp)
sub $t0, $sp,4
lw $t1, 704($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __getNode
add $t1, $sp, 520
lw $t0, 0($v0)
sw $t0, 0($t1)
lw $t0, 4($v0)
sw $t0, 4($t1)
lw $t0, 8($v0)
sw $t0, 8($t1)
lw $t0, 12($v0)
sw $t0, 12($t1)
lw $t0, 16($v0)
sw $t0, 16($t1)
lw $t0, 20($v0)
sw $t0, 20($t1)
lw $t0, 24($v0)
sw $t0, 24($t1)
lw $t0, 28($v0)
sw $t0, 28($t1)
lw $t0, 32($v0)
sw $t0, 32($t1)
lw $t0, 36($v0)
sw $t0, 36($t1)
lw $t0, 40($v0)
sw $t0, 40($t1)
lw $t0, 44($v0)
sw $t0, 44($t1)
lw $t0, 48($v0)
sw $t0, 48($t1)
lw $t0, 52($v0)
sw $t0, 52($t1)
lw $t0, 56($v0)
sw $t0, 56($t1)
lw $t0, 60($v0)
sw $t0, 60($t1)
lw $t0, 64($v0)
sw $t0, 64($t1)
lw $t0, 68($v0)
sw $t0, 68($t1)
lw $t0, 72($v0)
sw $t0, 72($t1)
lw $t0, 76($v0)
sw $t0, 76($t1)
lw $t0, 80($v0)
sw $t0, 80($t1)
lw $t0, 84($v0)
sw $t0, 84($t1)
lw $t0, 88($v0)
sw $t0, 88($t1)
lw $t0, 92($v0)
sw $t0, 92($t1)
lw $t0, 96($v0)
sw $t0, 96($t1)
lw $t0, 100($v0)
sw $t0, 100($t1)
lw $t0, 104($v0)
sw $t0, 104($t1)
lw $t0, 108($v0)
sw $t0, 108($t1)
lw $t0, 112($v0)
sw $t0, 112($t1)
add $sp, $sp, 52
lw $ra, 0($sp)
add $sp, $sp,8
add $t0,$sp,460
sw $t0, 456($sp)
lw $t0, 576($sp)
sw $t0, 448($sp)
lw $t0, 448($sp)
li $t1, 0
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 456($sp)
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 444($sp)
lw $t0, 0($t0)
sw $t0, 452($sp)
lw $t0, 448($sp)
lw $t1, 452($sp)
sw $t1, 0($t0)
lw $t0, 448($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 448($sp)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
add $t0,$sp,328
sw $t0, 324($sp)
sub $t0, $sp,4
lw $t1, 700($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __getNode
add $t1, $sp, 268
lw $t0, 0($v0)
sw $t0, 0($t1)
lw $t0, 4($v0)
sw $t0, 4($t1)
lw $t0, 8($v0)
sw $t0, 8($t1)
lw $t0, 12($v0)
sw $t0, 12($t1)
lw $t0, 16($v0)
sw $t0, 16($t1)
lw $t0, 20($v0)
sw $t0, 20($t1)
lw $t0, 24($v0)
sw $t0, 24($t1)
lw $t0, 28($v0)
sw $t0, 28($t1)
lw $t0, 32($v0)
sw $t0, 32($t1)
lw $t0, 36($v0)
sw $t0, 36($t1)
lw $t0, 40($v0)
sw $t0, 40($t1)
lw $t0, 44($v0)
sw $t0, 44($t1)
lw $t0, 48($v0)
sw $t0, 48($t1)
lw $t0, 52($v0)
sw $t0, 52($t1)
lw $t0, 56($v0)
sw $t0, 56($t1)
lw $t0, 60($v0)
sw $t0, 60($t1)
lw $t0, 64($v0)
sw $t0, 64($t1)
lw $t0, 68($v0)
sw $t0, 68($t1)
lw $t0, 72($v0)
sw $t0, 72($t1)
lw $t0, 76($v0)
sw $t0, 76($t1)
lw $t0, 80($v0)
sw $t0, 80($t1)
lw $t0, 84($v0)
sw $t0, 84($t1)
lw $t0, 88($v0)
sw $t0, 88($t1)
lw $t0, 92($v0)
sw $t0, 92($t1)
lw $t0, 96($v0)
sw $t0, 96($t1)
lw $t0, 100($v0)
sw $t0, 100($t1)
lw $t0, 104($v0)
sw $t0, 104($t1)
lw $t0, 108($v0)
sw $t0, 108($t1)
lw $t0, 112($v0)
sw $t0, 112($t1)
add $sp, $sp, 52
lw $ra, 0($sp)
add $sp, $sp,8
add $t0,$sp,208
sw $t0, 204($sp)
lw $t0, 324($sp)
sw $t0, 196($sp)
lw $t0, 196($sp)
li $t1, 0
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 204($sp)
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
lw $t0, 192($sp)
lw $t0, 0($t0)
sw $t0, 200($sp)
lw $t0, 196($sp)
lw $t1, 200($sp)
sw $t1, 0($t0)
lw $t0, 196($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 196($sp)
lw $t0, 192($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 192($sp)
add $t0,$sp,188
sw $t0, 184($sp)
lw $t0, 184($sp)
li $t1, 0
sw $t1, 0($t0)
add $t0,$sp,180
sw $t0, 176($sp)
lw $t0, 176($sp)
li $t1, 0
sw $t1, 0($t0)
___label26:
li $t0, 1
sw $t0, 172($sp)
lw $t0, 188($sp)
lw $t1, _size1
blt $t0,$t1,___label28
li $t0, 0
sw $t0, 172($sp)
___label28:
lw $t0, 172($sp)
li $t1, 0
beq $t0,$t1,___label25
___label30:
li $t0, 1
sw $t0, 168($sp)
lw $t0, 180($sp)
lw $t1, _size1
blt $t0,$t1,___label32
li $t0, 0
sw $t0, 168($sp)
___label32:
lw $t0, 168($sp)
li $t1, 0
beq $t0,$t1,___label29
add $t0,$sp,580
sw $t0, 160($sp)
lw $t0, 188($sp)
li $t1, 20
mul $t0, $t0, $t1
sw $t0, 152($sp)
lw $t0, 160($sp)
lw $t1, 152($sp)
add $t0, $t0, $t1
sw $t0, 152($sp)
lw $t0, 180($sp)
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 148($sp)
lw $t0, 152($sp)
lw $t1, 148($sp)
add $t0, $t0, $t1
sw $t0, 148($sp)
lw $t0, 148($sp)
lw $t0, 0($t0)
sw $t0, 144($sp)
add $t0,$sp,328
sw $t0, 140($sp)
lw $t0, 188($sp)
li $t1, 20
mul $t0, $t0, $t1
sw $t0, 132($sp)
lw $t0, 140($sp)
lw $t1, 132($sp)
add $t0, $t0, $t1
sw $t0, 132($sp)
lw $t0, 180($sp)
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 128($sp)
lw $t0, 132($sp)
lw $t1, 128($sp)
add $t0, $t0, $t1
sw $t0, 128($sp)
lw $t0, 128($sp)
lw $t0, 0($t0)
sw $t0, 124($sp)
li $t0, 1
sw $t0, 164($sp)
lw $t0, 144($sp)
lw $t1, 124($sp)
bgt $t0,$t1,___label35
li $t0, 0
sw $t0, 164($sp)
___label35:
lw $t0, 164($sp)
li $t1, 0
beq $t0,$t1,___label33
add $t0,$sp,580
sw $t0, 120($sp)
lw $t0, 120($sp)
sw $t0, 116($sp)
add $t0,$sp,328
sw $t0, 112($sp)
lw $t0, 112($sp)
sw $t0, 108($sp)
sub $t0, $sp,4
lw $t1, 116($sp)
sw $t1, 0($t0)
sub $t0, $sp,8
lw $t1, 108($sp)
sw $t1, 0($t0)
sub $sp, $sp,12
sw $ra, 0($sp)
jal __exchange
add $t1, $sp, 276
sw $v0, 0($t1)
add $sp, $sp, 160
lw $ra, 0($sp)
add $sp, $sp,12
add $t0,$sp,580
sw $t0, 100($sp)
lw $t0, 100($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 96($sp)
lw $t0, 96($sp)
lw $t0, 0($t0)
add $t1, $sp, 92
sw $t0, 0($t1)
lw $t0, 92($sp)
sw $t0, 88($sp)
add $t0,$sp,580
sw $t0, 84($sp)
lw $t0, 84($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 80($sp)
lw $t0, 80($sp)
lw $t0, 0($t0)
add $t1, $sp, 76
sw $t0, 0($t1)
lw $t0, 76($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 72($sp)
add $t0,$sp,580
sw $t0, 68($sp)
lw $t0, 68($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 64($sp)
lw $t0, 64($sp)
lw $t1, 72($sp)
sw $t1, 0($t0)
add $t0,$sp,328
sw $t0, 56($sp)
lw $t0, 56($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 52($sp)
lw $t0, 52($sp)
lw $t0, 0($t0)
add $t1, $sp, 48
sw $t0, 0($t1)
lw $t0, 48($sp)
sw $t0, 44($sp)
add $t0,$sp,328
sw $t0, 40($sp)
lw $t0, 40($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 36($sp)
lw $t0, 36($sp)
lw $t0, 0($t0)
add $t1, $sp, 32
sw $t0, 0($t1)
lw $t0, 32($sp)
li $t1, 1
sub $t0, $t0, $t1
sw $t0, 28($sp)
add $t0,$sp,328
sw $t0, 24($sp)
lw $t0, 24($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 20($sp)
lw $t0, 20($sp)
lw $t1, 28($sp)
sw $t1, 0($t0)
j ___label34
___label33:
___label34:
___label31:
lw $t0, 180($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 12($sp)
add $t0,$sp,180
sw $t0, 8($sp)
lw $t0, 8($sp)
lw $t1, 12($sp)
sw $t1, 0($t0)
j ___label30
___label29:
___label27:
lw $t0, 188($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 4($sp)
add $t0,$sp,188
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t1, 4($sp)
sw $t1, 0($t0)
j ___label26
___label25:
jr $ra
__show:
sub $sp, $sp, 144
add $t0,$sp,140
sw $t0, 136($sp)
lw $t0, 136($sp)
li $t1, 0
sw $t1, 0($t0)
___label38:
li $t0, 1
sw $t0, 132($sp)
lw $t0, 140($sp)
lw $t1, _size1
blt $t0,$t1,___label40
li $t0, 0
sw $t0, 132($sp)
___label40:
lw $t0, 132($sp)
li $t1, 0
beq $t0,$t1,___label37
add $t0,$sp,128
sw $t0, 124($sp)
lw $t0, 124($sp)
li $t1, 0
sw $t1, 0($t0)
___label42:
li $t0, 1
sw $t0, 120($sp)
lw $t0, 128($sp)
lw $t1, _size1
blt $t0,$t1,___label44
li $t0, 0
sw $t0, 120($sp)
___label44:
lw $t0, 120($sp)
li $t1, 0
beq $t0,$t1,___label41
sub $t0, $sp,4
li $t1, 20
sw $t1, 0($t0)
li $v0, 9
lw $a0, -4($sp)
syscall
sw $v0, __t137
li $t0, 37
lw $t1, __t137
sw $t0, 0($t1)
li $t0, 100
lw $t1, __t137
sw $t0, 4($t1)
li $t0, 92
lw $t1, __t137
sw $t0, 8($t1)
li $t0, 116
lw $t1, __t137
sw $t0, 12($t1)
li $t0, 0
lw $t1, __t137
sw $t0, 16($t1)
la $t0, _a
sw $t0, 116($sp)
lw $t0, 148($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 112($sp)
lw $t0, 116($sp)
lw $t1, 112($sp)
add $t0, $t0, $t1
sw $t0, 112($sp)
lw $t0, 140($sp)
li $t1, 20
mul $t0, $t0, $t1
sw $t0, 104($sp)
lw $t0, 112($sp)
lw $t1, 104($sp)
add $t0, $t0, $t1
sw $t0, 104($sp)
lw $t0, 128($sp)
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 100($sp)
lw $t0, 104($sp)
lw $t1, 100($sp)
add $t0, $t0, $t1
sw $t0, 100($sp)
lw $t0, 100($sp)
lw $t0, 0($t0)
sw $t0, 96($sp)
sub $t0, $sp,4
lw $t1, __t137
sw $t1, 0($t0)
sub $t0, $sp,8
lw $t1, 96($sp)
sw $t1, 0($t0)
lw $a2, -4($sp)
sub $a1, $sp, 8
sw $ra, -12($sp)
jal printf_loop
lw $ra, -12($sp)
___label43:
lw $t0, 128($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 88($sp)
add $t0,$sp,128
sw $t0, 84($sp)
lw $t0, 84($sp)
lw $t1, 88($sp)
sw $t1, 0($t0)
j ___label42
___label41:
sub $t0, $sp,4
li $t1, 12
sw $t1, 0($t0)
li $v0, 9
lw $a0, -4($sp)
syscall
sw $v0, __t147
li $t0, 92
lw $t1, __t147
sw $t0, 0($t1)
li $t0, 110
lw $t1, __t147
sw $t0, 4($t1)
li $t0, 0
lw $t1, __t147
sw $t0, 8($t1)
sub $t0, $sp,4
lw $t1, __t147
sw $t1, 0($t0)
lw $a2, -4($sp)
sub $a1, $sp, 8
sw $ra, -8($sp)
jal printf_loop
lw $ra, -8($sp)
___label39:
lw $t0, 140($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 76($sp)
add $t0,$sp,140
sw $t0, 72($sp)
lw $t0, 72($sp)
lw $t1, 76($sp)
sw $t1, 0($t0)
j ___label38
___label37:
sub $t0, $sp,4
li $t1, 60
sw $t1, 0($t0)
li $v0, 9
lw $a0, -4($sp)
syscall
sw $v0, __t151
li $t0, 37
lw $t1, __t151
sw $t0, 0($t1)
li $t0, 99
lw $t1, __t151
sw $t0, 4($t1)
li $t0, 32
lw $t1, __t151
sw $t0, 8($t1)
li $t0, 37
lw $t1, __t151
sw $t0, 12($t1)
li $t0, 99
lw $t1, __t151
sw $t0, 16($t1)
li $t0, 32
lw $t1, __t151
sw $t0, 20($t1)
li $t0, 37
lw $t1, __t151
sw $t0, 24($t1)
li $t0, 100
lw $t1, __t151
sw $t0, 28($t1)
li $t0, 44
lw $t1, __t151
sw $t0, 32($t1)
li $t0, 32
lw $t1, __t151
sw $t0, 36($t1)
li $t0, 37
lw $t1, __t151
sw $t0, 40($t1)
li $t0, 100
lw $t1, __t151
sw $t0, 44($t1)
li $t0, 92
lw $t1, __t151
sw $t0, 48($t1)
li $t0, 110
lw $t1, __t151
sw $t0, 52($t1)
li $t0, 0
lw $t1, __t151
sw $t0, 56($t1)
la $t0, _a
sw $t0, 68($sp)
lw $t0, 148($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 64($sp)
lw $t0, 68($sp)
lw $t1, 64($sp)
add $t0, $t0, $t1
sw $t0, 64($sp)
lw $t0, 64($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 60($sp)
li $t0, 0
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 56($sp)
lw $t0, 60($sp)
lw $t1, 56($sp)
add $t0, $t0, $t1
sw $t0, 56($sp)
lw $t0, 56($sp)
lw $t0, 0($t0)
sw $t0, 52($sp)
la $t0, _a
sw $t0, 48($sp)
lw $t0, 148($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 44($sp)
lw $t0, 48($sp)
lw $t1, 44($sp)
add $t0, $t0, $t1
sw $t0, 44($sp)
lw $t0, 44($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 40($sp)
li $t0, 1
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 36($sp)
lw $t0, 40($sp)
lw $t1, 36($sp)
add $t0, $t0, $t1
sw $t0, 36($sp)
lw $t0, 36($sp)
lw $t0, 0($t0)
sw $t0, 32($sp)
la $t0, _a
sw $t0, 28($sp)
lw $t0, 148($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 24($sp)
lw $t0, 28($sp)
lw $t1, 24($sp)
add $t0, $t0, $t1
sw $t0, 24($sp)
lw $t0, 24($sp)
lw $t0, 108($t0)
add $t1, $sp, 20
sw $t0, 0($t1)
la $t0, _a
sw $t0, 16($sp)
lw $t0, 148($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 12($sp)
lw $t0, 16($sp)
lw $t1, 12($sp)
add $t0, $t0, $t1
sw $t0, 12($sp)
lw $t0, 12($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 8($sp)
lw $t0, 8($sp)
lw $t0, 0($t0)
add $t1, $sp, 4
sw $t0, 0($t1)
sub $t0, $sp,4
lw $t1, __t151
sw $t1, 0($t0)
sub $t0, $sp,8
lw $t1, 52($sp)
sw $t1, 0($t0)
sub $t0, $sp,12
lw $t1, 32($sp)
sw $t1, 0($t0)
sub $t0, $sp,16
lw $t1, 20($sp)
sw $t1, 0($t0)
sub $t0, $sp,20
lw $t1, 4($sp)
sw $t1, 0($t0)
lw $a2, -4($sp)
sub $a1, $sp, 8
sw $ra, -24($sp)
jal printf_loop
lw $ra, -24($sp)
jr $ra
main:
sub $sp, $sp, 4
sw $ra, 0($sp)
jal _start
lw $ra, 0($sp)
add $sp, $sp, 4
sub $sp, $sp, 948
add $t0,$sp,944
sw $t0, 940($sp)
lw $t0, 940($sp)
li $t1, 7
sw $t1, 0($t0)
add $t0,$sp,936
sw $t0, 924($sp)
lw $t0, 924($sp)
li $t1, 0
sw $t1, 0($t0)
___label47:
li $t0, 1
sw $t0, 920($sp)
lw $t0, 936($sp)
lw $t1, _size2
blt $t0,$t1,___label49
li $t0, 0
sw $t0, 920($sp)
___label49:
lw $t0, 920($sp)
li $t1, 0
beq $t0,$t1,___label46
add $t0,$sp,932
sw $t0, 916($sp)
lw $t0, 916($sp)
li $t1, 0
sw $t1, 0($t0)
___label51:
li $t0, 1
sw $t0, 912($sp)
lw $t0, 932($sp)
lw $t1, _size1
blt $t0,$t1,___label53
li $t0, 0
sw $t0, 912($sp)
___label53:
lw $t0, 912($sp)
li $t1, 0
beq $t0,$t1,___label50
add $t0,$sp,928
sw $t0, 908($sp)
lw $t0, 908($sp)
li $t1, 0
sw $t1, 0($t0)
___label55:
li $t0, 1
sw $t0, 904($sp)
lw $t0, 928($sp)
lw $t1, _size1
blt $t0,$t1,___label57
li $t0, 0
sw $t0, 904($sp)
___label57:
lw $t0, 904($sp)
li $t1, 0
beq $t0,$t1,___label54
lw $t0, 932($sp)
li $t1, 5110
mul $t0, $t0, $t1
sw $t0, 900($sp)
lw $t0, 900($sp)
lw $t1, 928($sp)
add $t0, $t0, $t1
sw $t0, 896($sp)
li $t0, 34
lw $t1, 936($sp)
sub $t0, $t0, $t1
sw $t0, 892($sp)
lw $t0, 896($sp)
lw $t1, 892($sp)
div $t0, $t1
mfhi $t0
sw $t0, 888($sp)
lw $t0, 888($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 884($sp)
la $t0, _a
sw $t0, 880($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 876($sp)
lw $t0, 880($sp)
lw $t1, 876($sp)
add $t0, $t0, $t1
sw $t0, 876($sp)
lw $t0, 932($sp)
li $t1, 20
mul $t0, $t0, $t1
sw $t0, 868($sp)
lw $t0, 876($sp)
lw $t1, 868($sp)
add $t0, $t0, $t1
sw $t0, 868($sp)
lw $t0, 928($sp)
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 864($sp)
lw $t0, 868($sp)
lw $t1, 864($sp)
add $t0, $t0, $t1
sw $t0, 864($sp)
lw $t0, 864($sp)
lw $t1, 884($sp)
sw $t1, 0($t0)
lw $t0, 932($sp)
lw $t1, 932($sp)
mul $t0, $t0, $t1
sw $t0, 860($sp)
lw $t0, 860($sp)
lw $t1, 932($sp)
mul $t0, $t0, $t1
sw $t0, 856($sp)
lw $t0, 936($sp)
lw $t1, 856($sp)
add $t0, $t0, $t1
sw $t0, 852($sp)
la $t0, _a
sw $t0, 848($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 844($sp)
lw $t0, 848($sp)
lw $t1, 844($sp)
add $t0, $t0, $t1
sw $t0, 844($sp)
lw $t0, 844($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 840($sp)
li $t0, 0
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 836($sp)
lw $t0, 840($sp)
lw $t1, 836($sp)
add $t0, $t0, $t1
sw $t0, 836($sp)
lw $t0, 836($sp)
lw $t1, 852($sp)
sw $t1, 0($t0)
lw $t0, 928($sp)
lw $t1, 932($sp)
add $t0, $t0, $t1
sw $t0, 832($sp)
lw $t0, 832($sp)
lw $t1, 936($sp)
add $t0, $t0, $t1
sw $t0, 828($sp)
lw $t0, 828($sp)
li $t1, 1
sllv $t0, $t0, $t1
sw $t0, 824($sp)
la $t0, _a
sw $t0, 820($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 816($sp)
lw $t0, 820($sp)
lw $t1, 816($sp)
add $t0, $t0, $t1
sw $t0, 816($sp)
lw $t0, 816($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 812($sp)
li $t0, 1
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 808($sp)
lw $t0, 812($sp)
lw $t1, 808($sp)
add $t0, $t0, $t1
sw $t0, 808($sp)
lw $t0, 808($sp)
lw $t1, 824($sp)
sw $t1, 0($t0)
lw $t0, 932($sp)
not $t0, $t0
sw $t0, 804($sp)
lw $t0, 936($sp)
lw $t1, 804($sp)
add $t0, $t0, $t1
sw $t0, 800($sp)
lw $t0, 800($sp)
lw $t1, 928($sp)
or $t0, $t0, $t1
sw $t0, 796($sp)
la $t0, _a
sw $t0, 792($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 788($sp)
lw $t0, 792($sp)
lw $t1, 788($sp)
add $t0, $t0, $t1
sw $t0, 788($sp)
lw $t0, 788($sp)
li $t1, 112
add $t0, $t0, $t1
sw $t0, 784($sp)
lw $t0, 784($sp)
lw $t1, 796($sp)
sw $t1, 0($t0)
la $t0, _a
sw $t0, 776($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 772($sp)
lw $t0, 776($sp)
lw $t1, 772($sp)
add $t0, $t0, $t1
sw $t0, 772($sp)
lw $t0, 772($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 768($sp)
li $t0, 0
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 764($sp)
lw $t0, 768($sp)
lw $t1, 764($sp)
add $t0, $t0, $t1
sw $t0, 764($sp)
lw $t0, 764($sp)
lw $t0, 0($t0)
sw $t0, 760($sp)
lw $t0, 760($sp)
li $t1, 26
div $t0, $t1
mfhi $t0
sw $t0, 756($sp)
lw $t0, 756($sp)
li $t1, 97
add $t0, $t0, $t1
sw $t0, 752($sp)
la $t0, _a
sw $t0, 748($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 744($sp)
lw $t0, 748($sp)
lw $t1, 744($sp)
add $t0, $t0, $t1
sw $t0, 744($sp)
lw $t0, 744($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 740($sp)
li $t0, 0
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 736($sp)
lw $t0, 740($sp)
lw $t1, 736($sp)
add $t0, $t0, $t1
sw $t0, 736($sp)
lw $t0, 736($sp)
lw $t1, 752($sp)
sw $t1, 0($t0)
la $t0, _a
sw $t0, 732($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 728($sp)
lw $t0, 732($sp)
lw $t1, 728($sp)
add $t0, $t0, $t1
sw $t0, 728($sp)
lw $t0, 728($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 724($sp)
li $t0, 1
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 720($sp)
lw $t0, 724($sp)
lw $t1, 720($sp)
add $t0, $t0, $t1
sw $t0, 720($sp)
lw $t0, 720($sp)
lw $t0, 0($t0)
sw $t0, 716($sp)
lw $t0, 716($sp)
li $t1, 26
div $t0, $t1
mfhi $t0
sw $t0, 712($sp)
lw $t0, 712($sp)
li $t1, 65
add $t0, $t0, $t1
sw $t0, 708($sp)
la $t0, _a
sw $t0, 704($sp)
lw $t0, 936($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 700($sp)
lw $t0, 704($sp)
lw $t1, 700($sp)
add $t0, $t0, $t1
sw $t0, 700($sp)
lw $t0, 700($sp)
li $t1, 100
add $t0, $t0, $t1
sw $t0, 696($sp)
li $t0, 1
li $t1, 4
mul $t0, $t0, $t1
sw $t0, 692($sp)
lw $t0, 696($sp)
lw $t1, 692($sp)
add $t0, $t0, $t1
sw $t0, 692($sp)
lw $t0, 692($sp)
lw $t1, 708($sp)
sw $t1, 0($t0)
___label56:
lw $t0, 928($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 688($sp)
add $t0,$sp,928
sw $t0, 684($sp)
lw $t0, 684($sp)
lw $t1, 688($sp)
sw $t1, 0($t0)
j ___label55
___label54:
___label52:
lw $t0, 932($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 680($sp)
add $t0,$sp,932
sw $t0, 676($sp)
lw $t0, 676($sp)
lw $t1, 680($sp)
sw $t1, 0($t0)
j ___label51
___label50:
___label48:
lw $t0, 936($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 672($sp)
add $t0,$sp,936
sw $t0, 668($sp)
lw $t0, 668($sp)
lw $t1, 672($sp)
sw $t1, 0($t0)
j ___label47
___label46:
add $t0,$sp,932
sw $t0, 664($sp)
lw $t0, 664($sp)
li $t1, 0
sw $t1, 0($t0)
___label59:
li $t0, 1
sw $t0, 660($sp)
lw $t0, 932($sp)
lw $t1, _size2
blt $t0,$t1,___label61
li $t0, 0
sw $t0, 660($sp)
___label61:
lw $t0, 660($sp)
li $t1, 0
beq $t0,$t1,___label58
add $t0,$sp,928
sw $t0, 656($sp)
lw $t0, 656($sp)
li $t1, 0
sw $t1, 0($t0)
___label63:
li $t0, 1
sw $t0, 652($sp)
lw $t0, 928($sp)
lw $t1, _size2
blt $t0,$t1,___label65
li $t0, 0
sw $t0, 652($sp)
___label65:
lw $t0, 652($sp)
li $t1, 0
beq $t0,$t1,___label62
sub $t0, $sp,4
lw $t1, 932($sp)
sw $t1, 0($t0)
sub $t0, $sp,8
lw $t1, 928($sp)
sw $t1, 0($t0)
sub $sp, $sp,12
sw $ra, 0($sp)
jal __comp
add $t1, $sp, 1356
sw $v0, 0($t1)
add $sp, $sp, 696
lw $ra, 0($sp)
add $sp, $sp,12
___label64:
lw $t0, 928($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 644($sp)
add $t0,$sp,928
sw $t0, 640($sp)
lw $t0, 640($sp)
lw $t1, 644($sp)
sw $t1, 0($t0)
j ___label63
___label62:
___label60:
lw $t0, 932($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 636($sp)
add $t0,$sp,932
sw $t0, 632($sp)
lw $t0, 632($sp)
lw $t1, 636($sp)
sw $t1, 0($t0)
j ___label59
___label58:
add $t0,$sp,936
sw $t0, 628($sp)
lw $t0, 628($sp)
li $t1, 0
sw $t1, 0($t0)
___label67:
li $t0, 1
sw $t0, 624($sp)
lw $t0, 936($sp)
lw $t1, _size2
blt $t0,$t1,___label69
li $t0, 0
sw $t0, 624($sp)
___label69:
lw $t0, 624($sp)
li $t1, 0
beq $t0,$t1,___label66
sub $t0, $sp,4
lw $t1, 936($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __show
add $t1, $sp, 772
sw $v0, 0($t1)
add $sp, $sp, 144
lw $ra, 0($sp)
add $sp, $sp,8
___label68:
lw $t0, 936($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 616($sp)
add $t0,$sp,936
sw $t0, 612($sp)
lw $t0, 612($sp)
lw $t1, 616($sp)
sw $t1, 0($t0)
j ___label67
___label66:
lw $t0, _size2
li $t1, 1
sub $t0, $t0, $t1
sw $t0, 608($sp)
add $t0,$sp,932
sw $t0, 604($sp)
lw $t0, 604($sp)
lw $t1, 608($sp)
sw $t1, 0($t0)
___label71:
li $t0, 1
neg $t0, $t0
sw $t0, 596($sp)
li $t0, 1
sw $t0, 600($sp)
lw $t0, 932($sp)
lw $t1, 596($sp)
bgt $t0,$t1,___label73
li $t0, 0
sw $t0, 600($sp)
___label73:
lw $t0, 600($sp)
li $t1, 0
beq $t0,$t1,___label70
lw $t0, 932($sp)
li $t1, 3
div $t0, $t1
mfhi $t0
sw $t0, 588($sp)
li $t0, 1
sw $t0, 592($sp)
lw $t0, 588($sp)
li $t1, 0
beq $t0,$t1,___label76
li $t0, 0
sw $t0, 592($sp)
___label76:
lw $t0, 592($sp)
li $t1, 0
beq $t0,$t1,___label74
lw $t0, 932($sp)
li $t1, 3
add $t0, $t0, $t1
sw $t0, 584($sp)
lw $t0, 584($sp)
lw $t1, _size2
div $t0, $t1
mfhi $t0
sw $t0, 580($sp)
sub $t0, $sp,4
lw $t1, 580($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __getNode
add $t1, $sp, 524
lw $t0, 0($v0)
sw $t0, 0($t1)
lw $t0, 4($v0)
sw $t0, 4($t1)
lw $t0, 8($v0)
sw $t0, 8($t1)
lw $t0, 12($v0)
sw $t0, 12($t1)
lw $t0, 16($v0)
sw $t0, 16($t1)
lw $t0, 20($v0)
sw $t0, 20($t1)
lw $t0, 24($v0)
sw $t0, 24($t1)
lw $t0, 28($v0)
sw $t0, 28($t1)
lw $t0, 32($v0)
sw $t0, 32($t1)
lw $t0, 36($v0)
sw $t0, 36($t1)
lw $t0, 40($v0)
sw $t0, 40($t1)
lw $t0, 44($v0)
sw $t0, 44($t1)
lw $t0, 48($v0)
sw $t0, 48($t1)
lw $t0, 52($v0)
sw $t0, 52($t1)
lw $t0, 56($v0)
sw $t0, 56($t1)
lw $t0, 60($v0)
sw $t0, 60($t1)
lw $t0, 64($v0)
sw $t0, 64($t1)
lw $t0, 68($v0)
sw $t0, 68($t1)
lw $t0, 72($v0)
sw $t0, 72($t1)
lw $t0, 76($v0)
sw $t0, 76($t1)
lw $t0, 80($v0)
sw $t0, 80($t1)
lw $t0, 84($v0)
sw $t0, 84($t1)
lw $t0, 88($v0)
sw $t0, 88($t1)
lw $t0, 92($v0)
sw $t0, 92($t1)
lw $t0, 96($v0)
sw $t0, 96($t1)
lw $t0, 100($v0)
sw $t0, 100($t1)
lw $t0, 104($v0)
sw $t0, 104($t1)
lw $t0, 108($v0)
sw $t0, 108($t1)
lw $t0, 112($v0)
sw $t0, 112($t1)
add $sp, $sp, 52
lw $ra, 0($sp)
add $sp, $sp,8
add $t0,$sp,464
sw $t0, 460($sp)
la $t0, _a
sw $t0, 456($sp)
lw $t0, 932($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 452($sp)
lw $t0, 456($sp)
lw $t1, 452($sp)
add $t0, $t0, $t1
sw $t0, 452($sp)
lw $t0, 452($sp)
sw $t0, 444($sp)
lw $t0, 460($sp)
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
lw $t0, 440($sp)
lw $t0, 0($t0)
sw $t0, 448($sp)
lw $t0, 444($sp)
lw $t1, 448($sp)
sw $t1, 0($t0)
lw $t0, 444($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 444($sp)
lw $t0, 440($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 440($sp)
j ___label75
___label74:
sub $t0, $sp,4
lw $t1, 932($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __getNode
add $t1, $sp, 384
lw $t0, 0($v0)
sw $t0, 0($t1)
lw $t0, 4($v0)
sw $t0, 4($t1)
lw $t0, 8($v0)
sw $t0, 8($t1)
lw $t0, 12($v0)
sw $t0, 12($t1)
lw $t0, 16($v0)
sw $t0, 16($t1)
lw $t0, 20($v0)
sw $t0, 20($t1)
lw $t0, 24($v0)
sw $t0, 24($t1)
lw $t0, 28($v0)
sw $t0, 28($t1)
lw $t0, 32($v0)
sw $t0, 32($t1)
lw $t0, 36($v0)
sw $t0, 36($t1)
lw $t0, 40($v0)
sw $t0, 40($t1)
lw $t0, 44($v0)
sw $t0, 44($t1)
lw $t0, 48($v0)
sw $t0, 48($t1)
lw $t0, 52($v0)
sw $t0, 52($t1)
lw $t0, 56($v0)
sw $t0, 56($t1)
lw $t0, 60($v0)
sw $t0, 60($t1)
lw $t0, 64($v0)
sw $t0, 64($t1)
lw $t0, 68($v0)
sw $t0, 68($t1)
lw $t0, 72($v0)
sw $t0, 72($t1)
lw $t0, 76($v0)
sw $t0, 76($t1)
lw $t0, 80($v0)
sw $t0, 80($t1)
lw $t0, 84($v0)
sw $t0, 84($t1)
lw $t0, 88($v0)
sw $t0, 88($t1)
lw $t0, 92($v0)
sw $t0, 92($t1)
lw $t0, 96($v0)
sw $t0, 96($t1)
lw $t0, 100($v0)
sw $t0, 100($t1)
lw $t0, 104($v0)
sw $t0, 104($t1)
lw $t0, 108($v0)
sw $t0, 108($t1)
lw $t0, 112($v0)
sw $t0, 112($t1)
add $sp, $sp, 52
lw $ra, 0($sp)
add $sp, $sp,8
add $t0,$sp,324
sw $t0, 320($sp)
la $t0, _a
sw $t0, 316($sp)
lw $t0, 932($sp)
li $t1, 116
mul $t0, $t0, $t1
sw $t0, 312($sp)
lw $t0, 316($sp)
lw $t1, 312($sp)
add $t0, $t0, $t1
sw $t0, 312($sp)
lw $t0, 312($sp)
sw $t0, 304($sp)
lw $t0, 320($sp)
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
lw $t0, 300($sp)
lw $t0, 0($t0)
sw $t0, 308($sp)
lw $t0, 304($sp)
lw $t1, 308($sp)
sw $t1, 0($t0)
lw $t0, 304($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 304($sp)
lw $t0, 300($sp)
li $t1, 4
add $t0, $t0, $t1
sw $t0, 300($sp)
___label75:
___label72:
lw $t0, 932($sp)
sw $t0, 296($sp)
lw $t0, 932($sp)
li $t1, 1
sub $t0, $t0, $t1
sw $t0, 292($sp)
add $t0,$sp,932
sw $t0, 288($sp)
lw $t0, 288($sp)
lw $t1, 292($sp)
sw $t1, 0($t0)
j ___label71
___label70:
sub $t0, $sp,4
li $t1, 12
sw $t1, 0($t0)
li $v0, 9
lw $a0, -4($sp)
syscall
sw $v0, __t275
li $t0, 92
lw $t1, __t275
sw $t0, 0($t1)
li $t0, 110
lw $t1, __t275
sw $t0, 4($t1)
li $t0, 0
lw $t1, __t275
sw $t0, 8($t1)
sub $t0, $sp,4
lw $t1, __t275
sw $t1, 0($t0)
lw $a2, -4($sp)
sub $a1, $sp, 8
sw $ra, -8($sp)
jal printf_loop
lw $ra, -8($sp)
add $t0,$sp,932
sw $t0, 280($sp)
lw $t0, 280($sp)
li $t1, 0
sw $t1, 0($t0)
___label78:
li $t0, 1
sw $t0, 276($sp)
lw $t0, 932($sp)
lw $t1, _size2
blt $t0,$t1,___label80
li $t0, 0
sw $t0, 276($sp)
___label80:
lw $t0, 276($sp)
li $t1, 0
beq $t0,$t1,___label77
add $t0,$sp,928
sw $t0, 272($sp)
lw $t0, 272($sp)
li $t1, 0
sw $t1, 0($t0)
___label82:
li $t0, 1
sw $t0, 268($sp)
lw $t0, 928($sp)
lw $t1, _size2
blt $t0,$t1,___label84
li $t0, 0
sw $t0, 268($sp)
___label84:
lw $t0, 268($sp)
li $t1, 0
beq $t0,$t1,___label81
sub $t0, $sp,4
li $t1, 16
sw $t1, 0($t0)
li $v0, 9
lw $a0, -4($sp)
syscall
sw $v0, __t281
li $t0, 37
lw $t1, __t281
sw $t0, 0($t1)
li $t0, 100
lw $t1, __t281
sw $t0, 4($t1)
li $t0, 32
lw $t1, __t281
sw $t0, 8($t1)
li $t0, 0
lw $t1, __t281
sw $t0, 12($t1)
sub $t0, $sp,4
lw $t1, 932($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __getNode
add $t1, $sp, 212
lw $t0, 0($v0)
sw $t0, 0($t1)
lw $t0, 4($v0)
sw $t0, 4($t1)
lw $t0, 8($v0)
sw $t0, 8($t1)
lw $t0, 12($v0)
sw $t0, 12($t1)
lw $t0, 16($v0)
sw $t0, 16($t1)
lw $t0, 20($v0)
sw $t0, 20($t1)
lw $t0, 24($v0)
sw $t0, 24($t1)
lw $t0, 28($v0)
sw $t0, 28($t1)
lw $t0, 32($v0)
sw $t0, 32($t1)
lw $t0, 36($v0)
sw $t0, 36($t1)
lw $t0, 40($v0)
sw $t0, 40($t1)
lw $t0, 44($v0)
sw $t0, 44($t1)
lw $t0, 48($v0)
sw $t0, 48($t1)
lw $t0, 52($v0)
sw $t0, 52($t1)
lw $t0, 56($v0)
sw $t0, 56($t1)
lw $t0, 60($v0)
sw $t0, 60($t1)
lw $t0, 64($v0)
sw $t0, 64($t1)
lw $t0, 68($v0)
sw $t0, 68($t1)
lw $t0, 72($v0)
sw $t0, 72($t1)
lw $t0, 76($v0)
sw $t0, 76($t1)
lw $t0, 80($v0)
sw $t0, 80($t1)
lw $t0, 84($v0)
sw $t0, 84($t1)
lw $t0, 88($v0)
sw $t0, 88($t1)
lw $t0, 92($v0)
sw $t0, 92($t1)
lw $t0, 96($v0)
sw $t0, 96($t1)
lw $t0, 100($v0)
sw $t0, 100($t1)
lw $t0, 104($v0)
sw $t0, 104($t1)
lw $t0, 108($v0)
sw $t0, 108($t1)
lw $t0, 112($v0)
sw $t0, 112($t1)
add $sp, $sp, 52
lw $ra, 0($sp)
add $sp, $sp,8
add $t0,$sp,152
sw $t0, 148($sp)
sub $t0, $sp,4
lw $t1, 928($sp)
sw $t1, 0($t0)
sub $sp, $sp,8
sw $ra, 0($sp)
jal __getNode
add $t1, $sp, 92
lw $t0, 0($v0)
sw $t0, 0($t1)
lw $t0, 4($v0)
sw $t0, 4($t1)
lw $t0, 8($v0)
sw $t0, 8($t1)
lw $t0, 12($v0)
sw $t0, 12($t1)
lw $t0, 16($v0)
sw $t0, 16($t1)
lw $t0, 20($v0)
sw $t0, 20($t1)
lw $t0, 24($v0)
sw $t0, 24($t1)
lw $t0, 28($v0)
sw $t0, 28($t1)
lw $t0, 32($v0)
sw $t0, 32($t1)
lw $t0, 36($v0)
sw $t0, 36($t1)
lw $t0, 40($v0)
sw $t0, 40($t1)
lw $t0, 44($v0)
sw $t0, 44($t1)
lw $t0, 48($v0)
sw $t0, 48($t1)
lw $t0, 52($v0)
sw $t0, 52($t1)
lw $t0, 56($v0)
sw $t0, 56($t1)
lw $t0, 60($v0)
sw $t0, 60($t1)
lw $t0, 64($v0)
sw $t0, 64($t1)
lw $t0, 68($v0)
sw $t0, 68($t1)
lw $t0, 72($v0)
sw $t0, 72($t1)
lw $t0, 76($v0)
sw $t0, 76($t1)
lw $t0, 80($v0)
sw $t0, 80($t1)
lw $t0, 84($v0)
sw $t0, 84($t1)
lw $t0, 88($v0)
sw $t0, 88($t1)
lw $t0, 92($v0)
sw $t0, 92($t1)
lw $t0, 96($v0)
sw $t0, 96($t1)
lw $t0, 100($v0)
sw $t0, 100($t1)
lw $t0, 104($v0)
sw $t0, 104($t1)
lw $t0, 108($v0)
sw $t0, 108($t1)
lw $t0, 112($v0)
sw $t0, 112($t1)
add $sp, $sp, 52
lw $ra, 0($sp)
add $sp, $sp,8
add $t0,$sp,32
sw $t0, 28($sp)
sub $t0, $sp,116
lw $t1, 148($sp)
lw $t2, 0($t1)
sw $t2, 0($t0)
lw $t2, 4($t1)
sw $t2, 4($t0)
lw $t2, 8($t1)
sw $t2, 8($t0)
lw $t2, 12($t1)
sw $t2, 12($t0)
lw $t2, 16($t1)
sw $t2, 16($t0)
lw $t2, 20($t1)
sw $t2, 20($t0)
lw $t2, 24($t1)
sw $t2, 24($t0)
lw $t2, 28($t1)
sw $t2, 28($t0)
lw $t2, 32($t1)
sw $t2, 32($t0)
lw $t2, 36($t1)
sw $t2, 36($t0)
lw $t2, 40($t1)
sw $t2, 40($t0)
lw $t2, 44($t1)
sw $t2, 44($t0)
lw $t2, 48($t1)
sw $t2, 48($t0)
lw $t2, 52($t1)
sw $t2, 52($t0)
lw $t2, 56($t1)
sw $t2, 56($t0)
lw $t2, 60($t1)
sw $t2, 60($t0)
lw $t2, 64($t1)
sw $t2, 64($t0)
lw $t2, 68($t1)
sw $t2, 68($t0)
lw $t2, 72($t1)
sw $t2, 72($t0)
lw $t2, 76($t1)
sw $t2, 76($t0)
lw $t2, 80($t1)
sw $t2, 80($t0)
lw $t2, 84($t1)
sw $t2, 84($t0)
lw $t2, 88($t1)
sw $t2, 88($t0)
lw $t2, 92($t1)
sw $t2, 92($t0)
lw $t2, 96($t1)
sw $t2, 96($t0)
lw $t2, 100($t1)
sw $t2, 100($t0)
lw $t2, 104($t1)
sw $t2, 104($t0)
lw $t2, 108($t1)
sw $t2, 108($t0)
lw $t2, 112($t1)
sw $t2, 112($t0)
sub $t0, $sp,232
lw $t1, 28($sp)
lw $t2, 0($t1)
sw $t2, 0($t0)
lw $t2, 4($t1)
sw $t2, 4($t0)
lw $t2, 8($t1)
sw $t2, 8($t0)
lw $t2, 12($t1)
sw $t2, 12($t0)
lw $t2, 16($t1)
sw $t2, 16($t0)
lw $t2, 20($t1)
sw $t2, 20($t0)
lw $t2, 24($t1)
sw $t2, 24($t0)
lw $t2, 28($t1)
sw $t2, 28($t0)
lw $t2, 32($t1)
sw $t2, 32($t0)
lw $t2, 36($t1)
sw $t2, 36($t0)
lw $t2, 40($t1)
sw $t2, 40($t0)
lw $t2, 44($t1)
sw $t2, 44($t0)
lw $t2, 48($t1)
sw $t2, 48($t0)
lw $t2, 52($t1)
sw $t2, 52($t0)
lw $t2, 56($t1)
sw $t2, 56($t0)
lw $t2, 60($t1)
sw $t2, 60($t0)
lw $t2, 64($t1)
sw $t2, 64($t0)
lw $t2, 68($t1)
sw $t2, 68($t0)
lw $t2, 72($t1)
sw $t2, 72($t0)
lw $t2, 76($t1)
sw $t2, 76($t0)
lw $t2, 80($t1)
sw $t2, 80($t0)
lw $t2, 84($t1)
sw $t2, 84($t0)
lw $t2, 88($t1)
sw $t2, 88($t0)
lw $t2, 92($t1)
sw $t2, 92($t0)
lw $t2, 96($t1)
sw $t2, 96($t0)
lw $t2, 100($t1)
sw $t2, 100($t0)
lw $t2, 104($t1)
sw $t2, 104($t0)
lw $t2, 108($t1)
sw $t2, 108($t0)
lw $t2, 112($t1)
sw $t2, 112($t0)
sub $sp, $sp,236
sw $ra, 0($sp)
jal __compare
add $t1, $sp, 444
sw $v0, 0($t1)
add $sp, $sp, 184
lw $ra, 0($sp)
add $sp, $sp,236
sub $t0, $sp,4
lw $t1, __t281
sw $t1, 0($t0)
sub $t0, $sp,8
lw $t1, 24($sp)
sw $t1, 0($t0)
lw $a2, -4($sp)
sub $a1, $sp, 8
sw $ra, -12($sp)
jal printf_loop
lw $ra, -12($sp)
___label83:
lw $t0, 928($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 16($sp)
add $t0,$sp,928
sw $t0, 12($sp)
lw $t0, 12($sp)
lw $t1, 16($sp)
sw $t1, 0($t0)
j ___label82
___label81:
sub $t0, $sp,4
li $t1, 12
sw $t1, 0($t0)
li $v0, 9
lw $a0, -4($sp)
syscall
sw $v0, __t290
li $t0, 92
lw $t1, __t290
sw $t0, 0($t1)
li $t0, 110
lw $t1, __t290
sw $t0, 4($t1)
li $t0, 0
lw $t1, __t290
sw $t0, 8($t1)
sub $t0, $sp,4
lw $t1, __t290
sw $t1, 0($t0)
lw $a2, -4($sp)
sub $a1, $sp, 8
sw $ra, -8($sp)
jal printf_loop
lw $ra, -8($sp)
___label79:
lw $t0, 932($sp)
li $t1, 1
add $t0, $t0, $t1
sw $t0, 4($sp)
add $t0,$sp,932
sw $t0, 0($sp)
lw $t0, 0($sp)
lw $t1, 4($sp)
sw $t1, 0($t0)
j ___label78
___label77:
li $v0, 0
jr $ra
jr $ra
printf_loop:
lw $a0, 0($a2)
beq $a0, 0, printf_end
add $a2, $a2, 4
beq $a0, 92, printf_slash
beq $a0, '%', printf_fmt
li $v0, 11
syscall
b printf_loop
printf_end:
jr $ra
printf_fmt:
lw $a0, 0($a2)
add $a2, $a2, 4
beq $a0, 'd', printf_int
beq $a0, 's', printf_str
beq $a0, 'c', printf_char
beq $a0, '%', printf_char
beq $a0, '.', printf_width
beq $a0, '0', printf_width
b printf_loop
printf_int:
lw $a0, 0($a1)
sub $a1, $a1, 4
li $v0, 1
syscall
b printf_loop
printf_str:
lw $a3, 0($a1)
sub $a1, $a1, 4
printf_str_lb1:
lw $a0, 0($a3)
add $a3, $a3, 4
beq $a0, 0, printf_str_lb2
li $v0, 11
syscall
b printf_str_lb1
printf_str_lb2:
b printf_loop
printf_char:
lw $a0, 0($a1)
sub $a1, $a1, 4
li $v0, 11
syscall
b printf_loop
printf_width:
add $a2, $a2, 8
lw $a3, 0($a1)
sub $a1, $a1, 4
li $a0, 48
li $v0, 11
bgt $a3, 999, print_width_lb1
syscall
bgt $a3, 99, print_width_lb1
syscall
bgt $a3, 9, print_width_lb1
syscall
print_width_lb1:
move $a0, $a3
li $v0, 1
syscall
b printf_loop
printf_slash:
lw $a0, 0($a2)
add $a2, $a2, 4
beq $a0, 'n', printf_enter
beq $a0, 't', printf_tab
b printf_loop
printf_enter:
li $a0, 10
li $v0, 11
syscall
b printf_loop
printf_tab:
li $a0, 9
li $v0, 11
syscall
b printf_loop
