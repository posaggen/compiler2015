/** Target: Special solutions for the n-queens problem. (6<=n<=11)
 * Possible optimization: Dead code elimination, common expression,loop unrolling, inline function
 * REMARKS: Please check your output carefully, since you may output wrong solutions.
 *
**/

#include <stdio.h>
#include <stdlib.h>

int a, b;

int nextInt(){
	char cc = getchar();
	int u = 0;
	while (cc < '0' || cc > '9') cc = getchar();
	while (cc >= '0' && cc <= '9'){
		u = u * 10 + cc - 48;
		cc = getchar();
	}
	return u;
}

int main() {
	a = nextInt();
    b = nextInt();
    printf("%d\n", a + b);
    return 0;
}

