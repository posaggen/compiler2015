package compiler2015.main;

import java.io.*;
import java.util.*;

import org.antlr.v4.runtime.*;
import org.antlr.runtime.tree.*;

import compiler2015.ast.*;
import compiler2015.ir.*;
import compiler2015.main.MidTerm.MidTermListener;
import compiler2015.syntactic.CLexer;
import compiler2015.syntactic.CParser;
import compiler2015.syntactic.CParser.ProgramContext;
import compiler2015.semantic.*;
import compiler2015.translate.*;

public class Main {

	static class CListener extends BaseErrorListener {
		@Override
		public void syntaxError(Recognizer<?, ?> recognizer,
				Object offendingSymbol, int line, int charPositionInLine,
				String msg, RecognitionException e) {
			System.err.println("Parse Error " + line + " " + charPositionInLine
					+ " " + msg);
			e.printStackTrace();
			System.err.println("exit with 1");
			System.exit(1);
		}
	}

	static String fileName = "D:/workspace/src/compiler2015/main/test.c";
	static AST ast = null;

	public static void main(String args[]) throws Exception {

/*		PrintStream ps = new PrintStream(new FileOutputStream(
				"D:/workspace/src/compiler2015/main/output.s"));
		System.setOut(ps);*/
		syntactic();

		Semantic semantic = new Semantic();
		semantic.checkAST(ast);
		if (semantic.hasError())
			exit(1);

		IR ir = (new IRgen()).genAST(ast);
		//ir.show();

		CodeGenerator codeGen = new CodeGenerator();
		codeGen.generate(ir);

	}

	private static void syntactic() throws Exception {
/*
		ANTLRFileStream input = new ANTLRFileStream(fileName);
		CLexer lexer = new CLexer(input);
		CommonTokenStream stream = new CommonTokenStream(lexer);
		CParser parser = new CParser(stream);
*/
		ANTLRInputStream input = new ANTLRInputStream(System.in); 
		CLexer lexer = new CLexer(input);
		CommonTokenStream stream = new CommonTokenStream(lexer);
		CParser parser = new CParser(stream);

		lexer.removeErrorListeners();
		lexer.addErrorListener(new CListener());
		parser.removeErrorListeners();
		parser.addErrorListener(new CListener());

		ProgramContext context = parser.program();

		ast = context.ret;
		// ast.show(0);
	}

	private static void exit(int i) {
		System.err.println("exit with " + i);
		System.exit(i);
	}

};
