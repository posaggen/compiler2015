package compiler2015.semantic;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import compiler2015.ast.*;
import compiler2015.environment.*;
import compiler2015.util.*;

public class Semantic {

	private Environment env;
	private int loopCount = 0;
	private int errorCount = 0;
	private HashSet<String> map = new HashSet<String>();

	private boolean checkTypeEqual(Type a, Type b) {
		if (a instanceof IntType && !(b instanceof IntType))
			return false;
		if (a instanceof CharType && !(b instanceof CharType))
			return false;
		if (a instanceof PointerType && !(b instanceof PointerType))
			return false;
		if (a instanceof ArrayType && !(b instanceof ArrayType))
			return false;
		if (a instanceof ArrayType) {
			return checkTypeEqual(((ArrayType) a).baseType,
					((ArrayType) b).baseType);
		}
		if (a instanceof PointerType) {
			return checkTypeEqual(((PointerType) a).baseType,
					((PointerType) b).baseType);
		}
		if (a instanceof StructType) {
			if (b instanceof StructType) {
				if (((StructType) a).tag.equals(((StructType) b).tag))
					return true;
				else
					return false;
			} else
				return false;
		} else if (a instanceof UnionType) {
			if (b instanceof UnionType) {
				if (((UnionType) a).tag.equals(((UnionType) b).tag))
					return true;
				else
					return false;
			} else
				return false;
		}
		return true;
	}

	private Type arrayToPointer(Type returnType) {
		while (returnType instanceof ArrayType) {
			returnType = ((ArrayType) returnType).baseType;
		}
		returnType = new PointerType(returnType);
		return returnType;
	}

	private Type normal(Type type) {
		if (type instanceof FunctionType)
			return ((FunctionType) type).returnType;
		else
			return type;
	}

	private boolean legalCast(Type a, Type b) {
		a = normal(a);
		b = normal(b);
		if (a instanceof VoidType) {
			if (b instanceof VoidType)
				return true;
			else
				return false;
		}
		if (a instanceof StructType) {
			if (b instanceof StructType) {
				if (((StructType) a).tag.equals(((StructType) b).tag))
					return true;
				else
					return false;
			} else
				return false;
		} else if (a instanceof UnionType) {
			if (b instanceof UnionType) {
				if (((UnionType) a).tag.equals(((UnionType) b).tag))
					return true;
				else
					return false;
			} else
				return false;
		}
		return true;
	}

	private void legalAssign(Pair<Type, Integer> lType,
			Pair<Type, Integer> rType) {
		if ((lType.second & 1) == 0) {
			error("left part of assignment has to be lvalue");
		}
		Type tl = normal(lType.first);
		Type tr = normal(rType.first);
		if (tl instanceof VoidType || tr instanceof VoidType) {
			error("assignment statement with illegal operands");
		}
		if (tl instanceof StructType || tr instanceof StructType) {
			error("assignment statement with illegal operands");
		}
		if (tl instanceof UnionType || tr instanceof UnionType) {
			error("assignment statement with illegal operands");
		}
	}

	private void legalOp(Pair<Type, Integer> lType, Pair<Type, Integer> rType) {
		Type tl = normal(lType.first), tr = normal(rType.first);
		if (tl instanceof VoidType || tr instanceof VoidType) {
			error("binary op with illegal operands");
		}
		if (tl instanceof StructType || tr instanceof StructType) {
			error("binary op with illegal operands");
		}
		if (tl instanceof UnionType || tr instanceof UnionType) {
			error("binary op statement with illegal operands");
		}
	}

	private void checkTypeDeclared(Type type) {
		if (type instanceof StructType) {
			Object obj = env.getType(((StructType) type).tag);
			if (obj == null || !(obj instanceof StructDecl)) {
				error("undeclared struct type");
			}
		} else if (type instanceof UnionType) {
			Object obj = env.getType(((UnionType) type).tag);
			if (obj == null || !(obj instanceof UnionDecl)) {
				error("undeclared union type");
			}
		} else if (type instanceof ArrayType) {
			while (type instanceof ArrayType) {
				Pair<Type, Integer> pair = checkExpr(((ArrayType) type).arraySize);
				if (pair != null) {
					if (env.getLevel() == 0 && pair.second < 2) {
						error("array size is not const value");
					}
				} else {
					error("undeclared array type");
					return;
				}
				Expr expr = ((ArrayType) type).arraySize;
				if (expr instanceof UnaryExpr) {
					if (((UnaryExpr) expr).op == UnaryOp.MINUS) {
						error("negdef in array size");
					}
				}
				type = ((ArrayType) type).baseType;
			}
			checkTypeDeclared(type);
		}
	}

	public Semantic() {
		env = new Environment();
	}

	public void error(String err) {
		++errorCount;
		// System.err.println(err);
	}

	public boolean hasError() {
		return errorCount > 0;
	}

	public void checkAST(AST ast) {
		for (Decl element : ast.decls) {
			checkDecl(element);
		}
	}

	private void checkDecl(Decl element) {
		if (element instanceof FunctionDecl) {
			checkFunctionDecl((FunctionDecl) element);
		} else if (element instanceof StructDecl) {
			checkStructDecl((StructDecl) element);
		} else if (element instanceof UnionDecl) {
			checkUnionDecl((UnionDecl) element);
		} else if (element instanceof VarDecl) {
			checkVarDecl((VarDecl) element);
		}
	}

	private void checkFunctionDecl(FunctionDecl decl) {

		// type judge

		checkTypeDeclared(decl.returnType);

		// redeclaration judge

		if (decl.name.equals("printf") || decl.name.equals("malloc")
				|| decl.name.equals("getchar")) {
			error("illegal function name " + decl.name.toString());
		} else if (env.getIdenLevel(decl.name) == env.getLevel()) {
			error(decl.name.toString()
					+ " redeclared as another kind of symbol");
		}

		// add

		env.putIden(decl.name, decl);

		// params & body judge

		env.beginScope();

		for (VarDecl element : decl.params) {
			checkTypeDeclared(element.type);
			Symbol name = element.name;
			if (env.getLevel() == env.getIdenLevel(name)) {
				error(name.toString() + " redeclared as another param");
			}
			env.putIden(name, element.type);
		}

		checkCompoundStmt(decl.body, decl.returnType);

		env.endScope();
	}

	private void checkStructDecl(StructDecl decl) {

		// redeclaration judge

		Symbol name = decl.tag;
		if (env.getTypeLevel(name) == env.getLevel()) {
			error(name.toString() + " redeclared as another type");
		}

		// fields judge

		env.beginScope();

		for (VarDecl element : decl.fields) {
			checkTypeDeclared(element.type);
			if (env.getIdenLevel(element.name) == env.getLevel()) {
				error(element.name.toString()
						+ " redeclared as another symbol in fields");
			}
			env.putIden(element.name, element.type);
		}

		env.endScope();

		// add

		env.putType(name, decl);

	}

	private void checkUnionDecl(UnionDecl decl) {

		// redeclaration judge

		Symbol name = decl.tag;
		if (env.getTypeLevel(name) == env.getLevel()) {
			error(name.toString() + " redeclared as another type");
		}

		// fields judge

		env.beginScope();

		for (VarDecl element : decl.fields) {
			checkTypeDeclared(element.type);
			if (env.getIdenLevel(element.name) == env.getLevel()) {
				error(element.name.toString()
						+ " redeclared as another symbol in fields");
			}
			env.putIden(element.name, element.type);
		}

		env.endScope();

		// add

		env.putType(name, decl);

	}

	private void checkVarDecl(VarDecl decl) {

		// type judge

		checkTypeDeclared(decl.type);
		if (decl.type instanceof VoidType) {
			error("void type in VarDecl");
		}
		if (decl.type instanceof ArrayType) {
			Type t = decl.type;
			while (t instanceof ArrayType) {
				if (checkExpr(((ArrayType) t).arraySize).second < 2) {
					error("array size is not const");
				}
				t = ((ArrayType) t).baseType;
			}
		}

		// name judge

		Symbol name = decl.name;
		if (env.getIdenLevel(name) == env.getLevel()) {
			if (env.getLevel() == 0) {
				Type t = (Type) env.getIden(name);
				if (!checkTypeEqual(t, decl.type)) {
					error("conflicting type");
				}
				if (map.contains(name.toString()) && decl.init != null) {
					error(name.toString() + " declared as another symbol");
				}
			} else {
				error(name.toString() + " declared as another symbol");
			}
		}

		// add

		env.putIden(name, decl.type);

		// initializer judge

		if (decl.init != null) {
			if (env.getLevel() == 0) {
				map.add(name.toString());
			}
			checkInitializer(decl.init, decl.type);
		}

	}

	private void checkInitializer(Initializer init, Type type) {
		if (init instanceof InitValue) {
			checkInitValue((InitValue) init, type);
		} else if (init instanceof InitList) {
			checkInitList((InitList) init, type);
		}
	}

	private void checkInitValue(InitValue init, Type type) {
		Pair<Type, Integer> pair = checkExpr(init.expr);
		if (pair != null) {
			Type t = pair.first;
			if (!legalCast(type, t)) {
				error("error initvalue");
			}
			if (env.getLevel() == 0) {
				if (type instanceof IntType || type instanceof CharType) {
					if (pair.first instanceof ArrayType
							|| pair.first instanceof PointerType) {
						error("error initvalue");
					}
				}
			}
		} else
			error("error initvalue");
	}

	private void checkInitList(InitList init, Type type) {
		for (Initializer element : init.inits) {
			checkInitializer(element, type);
			if (type instanceof IntType || type instanceof CharType)
				return;
		}
	}

	private void checkStmt(Stmt stmt, Type type) {
		if (stmt instanceof BreakStmt) {
			checkBreakStmt((BreakStmt) stmt, type);
		} else if (stmt instanceof ContinueStmt) {
			checkContinueStmt((ContinueStmt) stmt, type);
		} else if (stmt instanceof IfStmt) {
			checkIfStmt((IfStmt) stmt, type);
		} else if (stmt instanceof ForLoop) {
			checkForLoop((ForLoop) stmt, type);
		} else if (stmt instanceof WhileLoop) {
			checkWhileLoop((WhileLoop) stmt, type);
		} else if (stmt instanceof ReturnStmt) {
			checkReturnStmt((ReturnStmt) stmt, type);
		} else if (stmt instanceof CompoundStmt) {
			checkCompoundStmt((CompoundStmt) stmt, type);
		} else if (stmt instanceof Expr) {
			checkExpr((Expr) stmt);
		}
	}

	private void checkBreakStmt(BreakStmt stmt, Type type) {
		if (loopCount == 0)
			error("continue statement is not in a loop");
	}

	private void checkContinueStmt(ContinueStmt stmt, Type type) {
		if (loopCount == 0)
			error("continue statement is not in a loop");
	}

	private void checkIfStmt(IfStmt stmt, Type type) {
		Pair<Type, Integer> pair = checkExpr(stmt.condition);
		if (pair == null)
			return;

		if (pair.first != null) {
			Type t = pair.first;
			if (!legalCast(t, new IntType())) {
				error("illegal type of condition in if statement");
			}
		}

		checkStmt(stmt.consequent, type);
		if (stmt.alternative != null) {
			checkStmt(stmt.alternative, type);
		}
	}

	private void checkForLoop(ForLoop flp, Type type) {
		++loopCount;
		checkExpr(flp.init);
		Pair<Type, Integer> pair = checkExpr(flp.condition);
		if (pair == null)
			return;

		if (pair.first != null) {
			Type t = pair.first;
			if (!legalCast(t, new IntType())) {
				error("illegal type of condition in for loop");
			}
		}
		checkExpr(flp.step);
		checkStmt(flp.body, type);
		--loopCount;
	}

	private void checkWhileLoop(WhileLoop wlp, Type type) {
		++loopCount;
		Pair<Type, Integer> pair = checkExpr(wlp.condition);
		if (pair == null)
			return;

		if (pair.first != null) {
			Type t = pair.first;
			if (!legalCast(t, new IntType())) {
				error("illegal type of condition in while loop");
			}
		}
		checkStmt(wlp.body, type);
		--loopCount;
	}

	private void checkReturnStmt(ReturnStmt stmt, Type type) {
		Pair<Type, Integer> t = checkExpr(stmt.expr);
		if (t == null)
			return;
		if (!legalCast(t.first, type)) {
			error("Error return type");
		}
	}

	private void checkCompoundStmt(CompoundStmt stmt, Type type) {
		env.beginScope();

		for (Decl element : stmt.decls) {
			checkDecl(element);
		}

		for (Stmt element : stmt.stats) {
			checkStmt(element, type);
		}

		env.endScope();
	}

	private Pair<Type, Integer> checkExpr(Expr expr) {
		if (expr instanceof EmptyExpr) {
			return checkEmptyExpr((EmptyExpr) expr);
		} else if (expr instanceof BinaryExpr) {
			return checkBinaryExpr((BinaryExpr) expr);
		} else if (expr instanceof UnaryExpr) {
			return checkUnaryExpr((UnaryExpr) expr);
		} else if (expr instanceof SizeofExpr) {
			return checkSizeofExpr((SizeofExpr) expr);
		} else if (expr instanceof CastExpr) {
			return checkCastExpr((CastExpr) expr);
		} else if (expr instanceof PointerAccess) {
			return checkPointerAccess((PointerAccess) expr);
		} else if (expr instanceof RecordAccess) {
			return checkRecordAccess((RecordAccess) expr);
		} else if (expr instanceof SelfIncrement) {
			return checkSelfIncrement((SelfIncrement) expr);
		} else if (expr instanceof SelfDecrement) {
			return checkSelfDecrement((SelfDecrement) expr);
		} else if (expr instanceof ArrayAccess) {
			return checkArrayAccess((ArrayAccess) expr);
		} else if (expr instanceof FunctionCall) {
			return checkFunctionCall((FunctionCall) expr);
		} else if (expr instanceof Identifier) {
			return checkIdentifier((Identifier) expr);
		} else if (expr instanceof IntConst) {
			return checkIntConst((IntConst) expr);
		} else if (expr instanceof CharConst) {
			return checkCharConst((CharConst) expr);
		} else {
			return checkStringConst((StringConst) expr);
		}
	}

	private Pair<Type, Integer> checkEmptyExpr(EmptyExpr expr) {
		return new Pair<Type, Integer>(null, 0);
	}

	private Pair<Type, Integer> checkBinaryExpr(BinaryExpr expr) {
		Pair<Type, Integer> lType = checkExpr(expr.left);
		Pair<Type, Integer> rType = checkExpr(expr.right);
		if (lType == null || rType == null)
			return null;

		Type tl = normal(lType.first);
		Type tr = normal(rType.first);

		// ,

		if (expr.op == BinaryOp.COMMA) {
			return rType;
		}

		// =

		if (expr.op == BinaryOp.ASSIGN) {
			if ((lType.second & 1) == 0) {
				error("left part of assignment has to be lvalue");
				return null;
			}
			if (!legalCast(tr, tl)) {
				error("assignment statement with illegal operands");
				return null;
			}
			return new Pair<Type, Integer>(tl, 0);
		}

		// *= /= %= <<= >>= ^= &= |=

		if (expr.op == BinaryOp.ASSIGN_MUL || expr.op == BinaryOp.ASSIGN_DIV
				|| expr.op == BinaryOp.ASSIGN_MOD
				|| expr.op == BinaryOp.ASSIGN_SHL
				|| expr.op == BinaryOp.ASSIGN_SHR
				|| expr.op == BinaryOp.ASSIGN_AND
				|| expr.op == BinaryOp.ASSIGN_XOR
				|| expr.op == BinaryOp.ASSIGN_OR) {
			legalAssign(lType, rType);
			if (tl instanceof PointerType || tr instanceof PointerType
					|| tr instanceof ArrayType) {
				error("assignment statement with illegal operands");
				return null;
			}
			return new Pair<Type, Integer>(tl, 0);
		}

		// +=

		if (expr.op == BinaryOp.ASSIGN_ADD) {
			legalAssign(lType, rType);
			if (tl instanceof PointerType
					&& (tr instanceof PointerType || tr instanceof ArrayType)) {
				error("assignment statement with illegal operands");
				return null;
			}
			return new Pair<Type, Integer>(tl, 0);
		}

		// -=

		if (expr.op == BinaryOp.ASSIGN_SUB) {
			legalAssign(lType, rType);
			if (!(tl instanceof PointerType)
					&& (tr instanceof PointerType || tr instanceof ArrayType)) {
				error("assignment statement with illegal operands");
				return null;
			}
			return new Pair<Type, Integer>(tl, 0);
		}

		// ||

		if (expr.op == BinaryOp.LOGICAL_OR) {
			legalOp(lType, rType);
			int t = lType.second & rType.second & 2;
			return new Pair<Type, Integer>(new IntType(), t);
		}

		// &&
		if (expr.op == BinaryOp.LOGICAL_AND) {
			legalOp(lType, rType);
			int t = lType.second & rType.second & 2;
			return new Pair<Type, Integer>(new IntType(), t);
		}

		// == != >= <= > <

		if (expr.op == BinaryOp.EQ || expr.op == BinaryOp.NE
				|| expr.op == BinaryOp.LT || expr.op == BinaryOp.GT
				|| expr.op == BinaryOp.LE || expr.op == BinaryOp.GE) {
			legalOp(lType, rType);
			int t = lType.second & rType.second & 2;
			return new Pair<Type, Integer>(new IntType(), t);
		}

		// | ^ & << >> * / %

		if (expr.op == BinaryOp.OR || expr.op == BinaryOp.AND
				|| expr.op == BinaryOp.XOR || expr.op == BinaryOp.SHL
				|| expr.op == BinaryOp.SHR || expr.op == BinaryOp.MUL
				|| expr.op == BinaryOp.DIV || expr.op == BinaryOp.MOD) {
			legalOp(lType, rType);
			if (tl instanceof PointerType || tr instanceof PointerType
					|| tl instanceof ArrayType || tr instanceof ArrayType) {
				error("assignment statement with illegal operands");
				return null;
			}
			int t = lType.second & rType.second & 2;
			return new Pair<Type, Integer>(new IntType(), t);
		}

		// +

		if (expr.op == BinaryOp.ADD) {
			legalOp(lType, rType);
			if ((tl instanceof PointerType || tl instanceof ArrayType)
					&& (tr instanceof PointerType || tr instanceof ArrayType)) {
				error("assignment statement with illegal operands");
				return null;
			}
			int t = lType.second & rType.second & 2;
			Type returnType = tl;
			if (!(returnType instanceof PointerType || returnType instanceof ArrayType)) {
				if (!(tr instanceof CharType))
					returnType = tr;
			}
			if (returnType instanceof ArrayType)
				returnType = arrayToPointer(returnType);
			return new Pair<Type, Integer>(returnType, t);
		}

		// -

		if (expr.op == BinaryOp.SUB) {
			legalOp(lType, rType);
			if (!(tl instanceof PointerType || tl instanceof ArrayType)
					&& (tr instanceof PointerType || tr instanceof ArrayType)) {
				error("assignment statement with illegal operands");
				return null;
			}
			Type returnType = tl;
			if (!(returnType instanceof PointerType || returnType instanceof ArrayType)) {
				if (!(tr instanceof CharType))
					returnType = tr;
			}
			if (returnType instanceof ArrayType)
				returnType = arrayToPointer(returnType);
			if ((tl instanceof PointerType || tl instanceof ArrayType)
					&& (tr instanceof PointerType || tr instanceof ArrayType)) {
				returnType = new IntType();
			}
			int t = lType.second & rType.second & 2;
			return new Pair<Type, Integer>(returnType, t);
		}

		return null;
	}

	private Pair<Type, Integer> checkUnaryExpr(UnaryExpr expr) {
		Pair<Type, Integer> type = checkExpr(expr.expr);
		if (type == null)
			return null;

		Type t = normal(type.first);

		// -- ++

		if (expr.op == UnaryOp.INC || expr.op == UnaryOp.DEC) {
			if ((type.second & 1) == 0) {
				error("lvalue required as ++ / -- operand");
				return null;
			}
			if (t instanceof VoidType || t instanceof StructType
					|| t instanceof UnionType) {
				error("assignment statement with illegal operands");
				return null;
			}
			return new Pair<Type, Integer>(t, 0);
		}

		// sizeof

		if (expr.op == UnaryOp.SIZEOF) {
			return new Pair<Type, Integer>(new IntType(), 2);
		}

		// &

		if (expr.op == UnaryOp.AMPERSAND) {
			if ((type.second & 1) == 0) {
				error("lvalue required as unary operation &");
				return null;
			}
			Type returnType = t;
			if (returnType instanceof ArrayType)
				returnType = arrayToPointer(returnType);
			returnType = new PointerType(returnType);
			return new Pair<Type, Integer>(returnType, 2);
		}

		// *

		if (expr.op == UnaryOp.ASTERISK) {
			if (!(t instanceof PointerType || t instanceof ArrayType)) {
				error("invalid type of unary operatiron *");
				return null;
			}
			Type returnType = t;
			if (returnType instanceof ArrayType)
				returnType = arrayToPointer(returnType);
			returnType = ((PointerType) returnType).baseType;
			if (returnType instanceof VoidType) {
				error("can not get value of void pointer");
				return null;
			}
			return new Pair<Type, Integer>(returnType, 1);
		}

		// + - ~

		if (expr.op == UnaryOp.PLUS || expr.op == UnaryOp.MINUS
				|| expr.op == UnaryOp.TILDE) {
			if (!(t instanceof IntType) && !(t instanceof CharType)) {
				error("Invalid type of unary operation + / -");
				return null;
			}
			return new Pair<Type, Integer>(t, 2);
		}

		// !

		if (expr.op == UnaryOp.NOT) {
			if (t instanceof StructType || t instanceof UnionType
					|| t instanceof VoidType) {
				error("Invalid type of unary operation !");
				return null;
			}
			return new Pair<Type, Integer>(new IntType(), 2);
		}

		return null;
	}

	private Pair<Type, Integer> checkSizeofExpr(SizeofExpr expr) {
		checkTypeDeclared(normal(expr.type));
		return new Pair<Type, Integer>(new IntType(), 2);
	}

	private Pair<Type, Integer> checkCastExpr(CastExpr expr) {
		checkTypeDeclared(expr.cast);
		Pair<Type, Integer> type = checkExpr(expr.expr);
		if (type == null)
			return null;

		if (!legalCast(type.first, expr.cast)) {
			error("illegal cast operation");
			return null;
		}
		int t = type.second & 2;
		return new Pair<Type, Integer>(expr.cast, t);
	}

	private Pair<Type, Integer> checkPointerAccess(PointerAccess expr) {
		Pair<Type, Integer> pair = checkExpr(expr.body);
		if (pair == null)
			return null;

		Type type = normal(pair.first);
		if (type instanceof ArrayType)
			type = arrayToPointer(type);
		if (type instanceof PointerType) {
			Type base = ((PointerType) type).baseType;
			if (base instanceof StructType || base instanceof UnionType) {
				if (base instanceof StructType) {
					StructDecl decl = (StructDecl) env
							.getType(((StructType) base).tag);
					if (decl != null) {
						for (VarDecl element : decl.fields) {
							if (element.name.equals(expr.attribute)) {
								return new Pair<Type, Integer>(element.type, 1);
							}
						}
					}
				} else {
					UnionDecl decl = (UnionDecl) env
							.getType(((UnionType) base).tag);
					if (decl != null) {
						for (VarDecl element : decl.fields) {
							if (element.name.equals(expr.attribute)) {
								return new Pair<Type, Integer>(element.type, 1);
							}
						}
					}
				}
			} else
				error("-> not a record type");
		} else {
			error("-> not a pointer");
		}
		return null;
	}

	private Pair<Type, Integer> checkRecordAccess(RecordAccess expr) {
		Pair<Type, Integer> pair = checkExpr(expr.body);
		if (pair == null)
			return null;

		Type type = normal(pair.first);
		if (type instanceof StructType) {
			StructDecl decl = (StructDecl) env.getType(((StructType) type).tag);
			if (decl != null) {
				for (VarDecl element : decl.fields) {
					if (element.name.equals(expr.attribute)) {
						return new Pair<Type, Integer>(element.type, 1);
					}
				}
			} else
				return null;
		} else if (type instanceof UnionType) {
			UnionDecl decl = (UnionDecl) env.getType(((UnionType) type).tag);
			if (decl != null) {
				for (VarDecl element : decl.fields) {
					if (element.name.equals(expr.attribute)) {
						return new Pair<Type, Integer>(element.type, 1);
					}
				}
			} else
				return null;
		} else
			error("not a Record Type");
		return null;
	}

	private Pair<Type, Integer> checkSelfIncrement(SelfIncrement expr) {
		Pair<Type, Integer> pair = checkExpr(expr.body);
		if (pair == null)
			return null;

		if ((pair.second & 1) == 0) {
			error("not lvalue for self increment");
			return null;
		}
		if (pair.first instanceof StructType || pair.first instanceof UnionType
				|| pair.first instanceof VoidType) {
			error("illegal type for self increment");
			return null;
		}
		return new Pair<Type, Integer>(pair.first, 0);
	}

	private Pair<Type, Integer> checkSelfDecrement(SelfDecrement expr) {
		Pair<Type, Integer> pair = checkExpr(expr.body);
		if (pair == null)
			return null;

		if ((pair.second & 1) == 0) {
			error("not lvalue for self decrement");
			return null;
		}
		if (pair.first instanceof StructType || pair.first instanceof UnionType
				|| pair.first instanceof VoidType) {
			error("illegal type for self decrement");
			return null;
		}
		return new Pair<Type, Integer>(pair.first, 0);
	}

	private Pair<Type, Integer> checkArrayAccess(ArrayAccess expr) {
		Pair<Type, Integer> body = checkExpr(expr.body);
		Pair<Type, Integer> subscript = checkExpr(expr.subscript);
		if (body == null || subscript == null)
			return null;

		Type tb = normal(body.first), ts = normal(subscript.first);

		if (!(tb instanceof ArrayType) && !(tb instanceof PointerType)) {
			error("not a array type in array access");
			return null;
		} else {
			if (!legalCast(ts, new IntType())) {
				error("wrong subscript");
				return null;
			}
			Type returnType;
			int t = 2;
			if (tb instanceof ArrayType) {
				returnType = ((ArrayType) tb).baseType;
				if (!(returnType instanceof ArrayType))
					t = 1;
			} else {
				t = 1;
				returnType = ((PointerType) tb).baseType;
			}
			return new Pair<Type, Integer>(returnType, t);
		}
	}

	private Pair<Type, Integer> checkFunctionCall(FunctionCall expr) {
		Object obj = env.getIden(expr.body);
		if (obj == null) {
			error("function does not exist");
			return null;
		}
		if (obj instanceof FunctionDecl) {
			FunctionDecl decl = (FunctionDecl) obj;
			if (decl.name.equals("printf")) {
				if (expr.args.size() == 0) {
					error("wrong arguments for function printf");
					return null;
				} else {
					for (int i = 0; i < expr.args.size(); ++i) {
						Pair<Type, Integer> pair = checkExpr(expr.args.get(i));
						if (pair.first instanceof VoidType) {
							error("wrong arguments for function printf");
							return null;
						}
						if (i == 0) {
							if (pair.first instanceof PointerType) {
								Type type = ((PointerType) pair.first).baseType;
								if (!(type instanceof VoidType)
										&& !(type instanceof CharType)) {
									error("wrong arguments for function printf");
									return null;
								}
							} else if (pair.first instanceof ArrayType) {
								Type type = ((ArrayType) pair.first).baseType;
								if (!(type instanceof CharType)) {
									error("wrong arguments for function printf");
									return null;
								}
							}
						}
						if (pair.first instanceof VoidType) {
							error("wrong arguments for function printf");
							return null;
						}
					}
					return new Pair<Type, Integer>(new IntType(), 0);
				}
			} else if (decl.name.equals("malloc")) {
				if (expr.args.size() != 1) {
					error("wrong arguments for function malloc");
					return null;
				}
				Type type = checkExpr(expr.args.get(0)).first;
				if (!(type instanceof IntType) && !(type instanceof CharType)) {
					error("wrong arguments for function malloc");
					return null;
				}
				return new Pair<Type, Integer>(new PointerType(new VoidType()),
						0);
			} else if (decl.name.equals("getchar")) {
				if (expr.args.size() > 0) {
					error("wrong arugments for function getchar");
					return null;
				}
				return new Pair<Type, Integer>(new IntType(), 0);
			} else {
				if (decl.params.size() == 0) {
					for (int i = 0; i < expr.args.size(); ++i) {
						Type type = (checkExpr(expr.args.get(i))).first;
						if (type instanceof VoidType) {
							error("wrong arguments for function");
							return null;
						}
					}
				} else {
					if (decl.params.size() != expr.args.size()) {
						error("wrong arguments for function");
						return null;
					}
					for (int i = 0; i < decl.params.size(); ++i) {
						Type t1 = (decl.params.get(i)).type;
						Pair<Type, Integer> pair = checkExpr(expr.args.get(i));
						if (pair == null)
							return null;
						Type t2 = pair.first;
						if (!legalCast(t2, t1)) {
							error("wrong arguments for function #2");
							return null;
						}
					}
				}
				return new Pair<Type, Integer>(decl.returnType, 0);
			}
		} else {
			error("Wrong function name");
			return null;
		}
	}

	private Pair<Type, Integer> checkIdentifier(Identifier expr) {
		Object t = env.getIden(expr.symbol);
		if (t instanceof IntType || t instanceof CharType
				|| t instanceof PointerType || t instanceof UnionType
				|| t instanceof StructType) {
			return new Pair<Type, Integer>((Type) t, 1);
		} else if (t instanceof ArrayType) {
			return new Pair<Type, Integer>((Type) t, 2);
		} else if (t instanceof FunctionDecl) {
			error("Function used as a symbol");
			return null;
		}
		return null;
	}

	private Pair<Type, Integer> checkIntConst(IntConst expr) {
		return new Pair<Type, Integer>(new IntType(), 2);
	}

	private Pair<Type, Integer> checkCharConst(CharConst expr) {
		return new Pair<Type, Integer>(new CharType(), 2);
	}

	private Pair<Type, Integer> checkStringConst(StringConst expr) {
		return new Pair<Type, Integer>(new ArrayType(new CharType(),
				new IntConst(expr.value.length())), 2);
	}

}
