grammar C;

@header{
	package compiler2015.syntactic;

	import compiler2015.ast.*;
	import java.util.LinkedList;
	import java.util.List;
}

@parser::members{
	private List<Decl> curDecls;
	private List<VarDecl> curFields;
	private Type curType;
	private boolean isUnion;
	private boolean isChar;
	private int countID = 0;
	private List<Expr> postfixArgs;
	private int postfixType;
	private Symbol postfixSymbol;
	private Expr postfixExpr;
}

program returns [AST ret] 
		: {$ret = new AST();}( {curDecls = $ret.decls;} dclr = declaration  
		|   {curDecls = $ret.decls;} fdef = function_definition  )+ 
		;

declaration :  ts = type_specifier {curType = $ts.ret;} init_declarators? ';' 
			;

function_definition :
					 {List<VarDecl> para = new LinkedList<VarDecl>();}
					 ts = type_specifier {curType = $ts.ret;}
					 pd = plain_declarator
					 '(' (pa = parameters {para = $pa.ret;})? ')' 
					 cs = compound_statement 
					 {
						 curDecls.add(new FunctionDecl($pd.ret.type, $pd.ret.name, para, $cs.ret));
					 }
					;

parameters returns [List<VarDecl> ret]
				 :{$ret = new LinkedList<VarDecl>();} 
				  pd1 = plain_declaration {$ret.add($pd1.ret);} 
				  (',' pd2 = plain_declaration {$ret.add($pd2.ret);})* 
				 ;
 
declarators : d1 = declarator {curFields.add($d1.ret);}
			  (',' d2 = declarator {curFields.add($d2.ret);})*
		    ;
 
init_declarators : init_declarator (',' init_declarator)*
				 ;
 
init_declarator :
				 {Initializer inir = null;}
				 decl = declarator ('=' init = initializer {inir = $init.ret;} )? 
				 {
					 $decl.ret.init = inir;
					 curDecls.add($decl.ret);
			     }
			    ;
 
initializer returns [Initializer ret]
			: ae = assignment_expression {$ret = new InitValue($ae.ret);}
            | {List<Initializer> tmplist = new LinkedList<Initializer>();}
			  '{' inir1 = initializer {tmplist.add($inir1.ret);} (',' inir2 = initializer {tmplist.add($inir2.ret);} )* '}'
			  {$ret = new InitList(tmplist);}
			;
 
type_specifier returns [Type ret]
			   : {$ret = new VoidType();} 'void' 
			   | {$ret = new CharType();} 'char' 
			   | {$ret = new IntType();} 'int'
               | {
				   String id = "#T_T#" + (Integer)(countID++);
				   List<VarDecl> fields = new LinkedList<VarDecl>();
				   List<VarDecl> oldFields = curFields;
				   curFields = fields;
				 }
				 sou = struct_or_union 
				 (id1 = Identifier {--countID; id =$id1.text;})? 
				 '{' (ts = type_specifier {curType = $ts.ret;} declarators ';')+ '}'
				 {
					 curFields = oldFields;
					 if ($sou.ret){
						curDecls.add(new UnionDecl(Symbol.get(id), fields));
						$ret = new UnionType(Symbol.get(id));
				     } else {
						curDecls.add(new StructDecl(Symbol.get(id), fields));
						$ret = new StructType(Symbol.get(id));
				     }
			     }
               | sou1 = struct_or_union id2 = Identifier
				 { 
				   if ($sou1.ret){
				       $ret = new UnionType(Symbol.get($id2.text));
				   } else {
					   $ret = new StructType(Symbol.get($id2.text));
				   }
			     }
			   ;
 
struct_or_union returns [boolean ret]
                : 'struct' {$ret = false;}
				| 'union' {$ret = true;}
				;
 
plain_declaration returns [VarDecl ret] 
				  : ts = type_specifier {curType = $ts.ret;} decl = declarator
				    {
						$ret = $decl.ret;
					}
				  ;
 
declarator returns [VarDecl ret]
		   : {List<Expr> list = new LinkedList<Expr>();}
		     pd = plain_declarator 
		     ('[' ce = constant_expression ']' {list.add($ce.ret);})*
			 {
				 for (int i = list.size() - 1; i >= 0; --i){
					$pd.ret.type = new ArrayType($pd.ret.type, list.get(i));
				 }
			 }
			 {$ret = $pd.ret;}
		   ;
 
plain_declarator returns [VarDecl ret]
				 : {$ret = new VarDecl(); $ret.type = curType;} 
				   ('*' {$ret.type = new PointerType($ret.type);})* id = Identifier 
				   {$ret.name = Symbol.get($id.text);}
				 ;

// statements

statement returns [Stmt ret]
		  : es = expression_statement
			{$ret = $es.ret;}
          | cs = compound_statement
			{$ret = $cs.ret;}
          | ss = selection_statement
			{$ret = $ss.ret;}
          | is = iteration_statement
			{$ret = $is.ret;}
          | js = jump_statement 
			{$ret = $js.ret;}
		  ;
 
expression_statement returns [Expr ret]
					 : {$ret = new EmptyExpr();}
					   (expr = expression {$ret = $expr.ret;})? ';' 
				     ;
 
compound_statement returns [CompoundStmt ret]
				   : {
					   List<Decl> decls = new LinkedList<Decl>();
					   List<Decl> oldDecls = curDecls;
					   curDecls = decls;

					   List<Stmt> stats = new LinkedList<Stmt>();
					 }
				     '{' 
					 declaration* 
					 (stmt = statement {stats.add($stmt.ret);})* 
					 '}' 
					 {
						 curDecls = oldDecls;
						 $ret = new CompoundStmt(decls, stats);
					 }
				   ;
 
selection_statement returns [IfStmt ret]
					:   {$ret = new IfStmt();}
						'if' '(' expr = expression ')' stmt = statement ('else' stmt1 = statement {$ret.alternative = $stmt1.ret;})? 
						{$ret.condition = $expr.ret; $ret.consequent = $stmt.ret;}
					;
 
iteration_statement returns [Stmt ret]
				    : {WhileLoop t = new WhileLoop();}
					  'while' '(' expr = expression ')' stmt = statement
					  {t.condition = $expr.ret; t.body = $stmt.ret;}
					  {$ret = t;}
                    | {
						ForLoop t = new ForLoop();
						t.init = new EmptyExpr();
						t.condition = new EmptyExpr();
						t.step = new EmptyExpr();
					  }
					  'for' '(' (expr1 = expression {t.init = $expr1.ret;}) ? ';' 
								 (expr2 = expression {t.condition = $expr2.ret;}) ? ';' 
								 (expr3 = expression {t.step = $expr3.ret;}) ? ')' 
					  stmt = statement
					  {t.body = $stmt.ret;}
					  {$ret = t;}
					;
 
jump_statement returns [Stmt ret]
			   : 'continue' ';'
			     {$ret = new ContinueStmt();}
               | 'break' ';'
				 {$ret = new BreakStmt();}
               | {ReturnStmt t = new ReturnStmt(new EmptyExpr());}
			     'return' (expr = expression {t.expr = $expr.ret;})? ';'
				 {$ret = t;}
			   ;
			   
// Expressions

expression returns [Expr ret]
		   : ae1 = assignment_expression {$ret = $ae1.ret;}
		     (',' ae2 = assignment_expression {$ret = new BinaryExpr($ret, BinaryOp.COMMA, $ae2.ret);})* 
			 ;
 
assignment_expression returns [Expr ret]
					  : loe = logical_or_expression {$ret = $loe.ret;}
                      | ue = unary_expression 
					    ao = assignment_operator 
						ae = assignment_expression 
						{$ret = new BinaryExpr($ue.ret, $ao.ret, $ae.ret);}
					  ;
 
assignment_operator returns [BinaryOp ret]
					: '='     {$ret = BinaryOp.ASSIGN;}
					| '*='	  {$ret = BinaryOp.ASSIGN_MUL;}
					| '/='	  {$ret = BinaryOp.ASSIGN_DIV;}
					| '%='	  {$ret = BinaryOp.ASSIGN_MOD;}
					| '+='    {$ret = BinaryOp.ASSIGN_ADD;}
					| '-='    {$ret = BinaryOp.ASSIGN_SUB;}
					| '<<='   {$ret = BinaryOp.ASSIGN_SHL;}
					| '>>='   {$ret = BinaryOp.ASSIGN_SHR;}
					| '&='    {$ret = BinaryOp.ASSIGN_AND;}
					| '^='    {$ret = BinaryOp.ASSIGN_XOR;}
					| '|='    {$ret = BinaryOp.ASSIGN_OR;}
					;
 
constant_expression returns [Expr ret]
				    : loe = logical_or_expression {$ret = $loe.ret;}
					;
 
logical_or_expression returns [Expr ret]
					  : loe = logical_and_expression {$ret = $loe.ret;}
					    ('||' loe1 = logical_and_expression {$ret = new BinaryExpr($ret, BinaryOp.LOGICAL_OR, $loe1.ret);} )* 
					  ;
 
logical_and_expression returns [Expr ret]
					   : ioe = inclusive_or_expression {$ret = $ioe.ret;}
					     ('&&' ioe1 = inclusive_or_expression {$ret = new BinaryExpr($ret, BinaryOp.LOGICAL_AND, $ioe1.ret);} )* 
					   ;
 
inclusive_or_expression returns [Expr ret]
						: eoe = exclusive_or_expression {$ret = $eoe.ret;}
						  ('|' eoe1 = exclusive_or_expression {$ret = new BinaryExpr($ret, BinaryOp.OR, $eoe1.ret);} )* 
						;
 
exclusive_or_expression returns [Expr ret]
					    : ae = and_expression {$ret = $ae.ret;}
						  ('^' ae1 = and_expression {$ret = new BinaryExpr($ret, BinaryOp.XOR, $ae1.ret);} )* 
						;
 
and_expression returns [Expr ret]
			   : ee = equality_expression {$ret = $ee.ret;}
			     ('&' ee1 = equality_expression {$ret = new BinaryExpr($ret, BinaryOp.AND, $ee1.ret);} )* 
			   ;
 
equality_expression returns [Expr ret] 
					: re = relational_expression {$ret = $re.ret;}
					  (eo = equality_operator re1 = relational_expression {$ret = new BinaryExpr($ret, $eo.ret, $re1.ret);} )* 
					;
 
equality_operator returns [BinaryOp ret]
				  : '=='  {$ret = BinaryOp.EQ;}
				  | '!='  {$ret = BinaryOp.NE;}
				  ;
 
relational_expression returns [Expr ret]
					  : se = shift_expression {$ret = $se.ret;}
					    (ro = relational_operator se1 = shift_expression {$ret = new BinaryExpr($ret, $ro.ret, $se1.ret);} )* 
					  ;
 
relational_operator returns [BinaryOp ret]
					: '<'   {$ret = BinaryOp.LT;}
					| '>'	{$ret = BinaryOp.GT;}
					| '<='  {$ret = BinaryOp.LE;}
					| '>='  {$ret = BinaryOp.GE;}
					;
 
shift_expression returns [Expr ret]
				 : ae = additive_expression {$ret = $ae.ret;}
				   (so = shift_operator ae1 = additive_expression {$ret = new BinaryExpr($ret, $so.ret, $ae1.ret);} )* 
				 ;
 
shift_operator returns [BinaryOp ret]
			   : '<<'  {$ret = BinaryOp.SHL;}
			   | '>>'  {$ret = BinaryOp.SHR;}
			   ;
 
additive_expression returns [Expr ret]
                    : me = multiplicative_expression {$ret = $me.ret;}
					  (ao = additive_operator me1 = multiplicative_expression {$ret = new BinaryExpr($ret, $ao.ret, $me1.ret);} )* 
					;
 
additive_operator returns [BinaryOp ret]
				  : '+'  {$ret = BinaryOp.ADD;}
				  | '-'  {$ret = BinaryOp.SUB;}
				  ;
 
multiplicative_expression returns [Expr ret]
						  : ce = cast_expression {$ret = $ce.ret;}
						    (mo = multiplicative_operator ce1 = cast_expression {$ret = new BinaryExpr($ret, $mo.ret, $ce1.ret);} )* 
						  ;
 
multiplicative_operator returns [BinaryOp ret]
						: '*'  {$ret = BinaryOp.MUL;}
						| '/'  {$ret = BinaryOp.DIV;}
						| '%'  {$ret = BinaryOp.MOD;}
						;
 
cast_expression returns [Expr ret]
				: ue = unary_expression {$ret = $ue.ret;}
                | '(' tn = type_name ')' ce = cast_expression {$ret = new CastExpr($tn.ret, $ce.ret);}
				;

type_name returns [Type ret]
		  : ts = type_specifier {$ret = $ts.ret;} ('*' {$ret = new PointerType($ret);})*  
		  ;
 
unary_expression returns [Expr ret]
				 : pe = postfix_expression {$ret = $pe.ret;}
                 | '++' ue = unary_expression {$ret = new UnaryExpr(UnaryOp.INC, $ue.ret);}
                 | '--' ue = unary_expression {$ret = new UnaryExpr(UnaryOp.DEC, $ue.ret);}
                 | uo = unary_operator ce = cast_expression {$ret = new UnaryExpr($uo.ret, $ce.ret);}
                 | 'sizeof' ue = unary_expression  {$ret = new UnaryExpr(UnaryOp.SIZEOF, $ue.ret);}
                 | 'sizeof' '(' tn = type_name ')' {$ret = new SizeofExpr($tn.ret);} 
				 ;
 
unary_operator returns [UnaryOp ret]
			   : '&' {$ret = UnaryOp.AMPERSAND;}
			   | '*' {$ret = UnaryOp.ASTERISK;}
			   | '+' {$ret = UnaryOp.PLUS;}
			   | '-' {$ret = UnaryOp.MINUS;}
			   | '~' {$ret = UnaryOp.TILDE;}
			   | '!' {$ret = UnaryOp.NOT;}
			   ;
 
postfix_expression returns [Expr ret]
				   : pe = primary_expression
					 {
						 $ret = $pe.ret;
						 Symbol tmp = null;
						 if (postfixSymbol != null) tmp = Symbol.get(postfixSymbol.toString());
					 }
				     (postfix 
				     {
						 if (postfixType == 0){
							$ret = new ArrayAccess($ret, postfixExpr);
						 } else if (postfixType == 1){
							$ret = new FunctionCall(tmp, postfixArgs);
						 } else if (postfixType == 2){
							$ret = new RecordAccess($ret, postfixSymbol);
						 } else if (postfixType == 3){
							$ret = new PointerAccess($ret, postfixSymbol);
					     } else if (postfixType == 4){
							$ret = new SelfIncrement($ret);
						 } else {
							$ret = new SelfDecrement($ret);
					     }
					 }
					 )*
				   ;
 
postfix : '[' expr = expression ']' {postfixType = 0; postfixExpr = $expr.ret;}
        | {postfixArgs = new LinkedList<Expr>();} '(' (args = arguments {postfixArgs = $args.ret;} )? ')' {postfixType = 1; }
        | '.' id = Identifier		{postfixType = 2; postfixSymbol = Symbol.get($id.text);}
        | '->' id1 = Identifier	    {postfixType = 3; postfixSymbol = Symbol.get($id1.text);}
        | '++'						{postfixType = 4;}
        | '--'						{postfixType = 5;}
		;
 
arguments returns [List<Expr> ret] 
		  : {$ret = new LinkedList<Expr>();}
			ae = assignment_expression {$ret.add($ae.ret);}
			(',' ae1 = assignment_expression {$ret.add($ae1.ret);})* 
		  ;
 
primary_expression returns [Expr ret]
				   : id = Identifier {$ret = new Identifier(Symbol.get($id.text)); postfixSymbol = Symbol.get($id.text);}
                   | ic = integer_constant {$ret = $ic.ret;}
				   | cc = character_constant {$ret = $cc.ret;}
                   | st = STRINGLITERAL {$ret = new StringConst(new StringBuilder($st.text).toString());}
                   | '(' expr = expression ')' {$ret = $expr.ret;} 
				   ;

integer_constant returns[IntConst ret]
				 : Hex {$ret = new IntConst(Integer.parseInt($Hex.getText().substring(2), 16));}
				 | Dec {$ret = new IntConst($Dec.int);}
				 | Oct {$ret = new IntConst(Integer.parseInt($Oct.getText(), 8));}
				 ;

character_constant returns[CharConst ret]
				   : c=CHARACTERLITERAL {$ret=new CharConst(new StringBuilder($c.text).toString());}
				   ;




// LEXER =====================================================

Whitespace : [ \r\t\n]+ -> channel(HIDDEN);

fragment EOL : '\r' | '\n' | ('\r''\n') ;

Multi_comment : '/*' .*? '*/' -> channel(HIDDEN);
Single_comment : '//' ~[\r\n]*  -> channel(HIDDEN);
Preprocessing : '#' ~[\r\n]*  -> channel(HIDDEN);

Hex : '0' ('x'|'X') HexDigit+ ;
Dec : '0' | ([1-9] Digit*)  ;
Oct : '0' OctDigit+  ;

fragment Digit : [0-9];
fragment HexDigit : (Digit | [a-f] | [A-F]) ;
fragment OctDigit : [0-7] ;
fragment Letter: '$' | [A-Z] | '_' |  [a-z];

CHARACTERLITERAL : '\'' ( ('\\' ('b'|'t'|'n'|'f'|'r'| '0' |'\"'|'\''|'\\')) 
	| ~('\''|'\\') ) '\'' 
	| '\'\\' OctDigit OctDigit OctDigit '\''
	| '\'\\' ('0x'|'0X') HexDigit HexDigit'\'';
STRINGLITERAL : '"' ( ('\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')) | ~('\\'|'"') )* '"' ;

Identifier : Letter (Letter | Digit)*;



