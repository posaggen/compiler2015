package compiler2015.util;

import compiler2015.ast.*;
import compiler2015.ir.*;

public class All{
	public Type type;
	public Address addr; 
	
	public All(){
		type = null;
		addr = null;
	}
	
	public All(Type _type, Address _addr){
		type = _type;
		addr = _addr;
	}
}