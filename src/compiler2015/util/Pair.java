package compiler2015.util;

public class Pair<t1, t2> {
	public t1 first;
	public t2 second;

	public Pair(t1 x, t2 y) {
		this.first = x;
		this.second = y;
	}
}