package compiler2015.ast;

public abstract class BasicType extends Type {
	public abstract void show(int indent);
}
