package compiler2015.ast;

public class UnionType extends BasicType {
    public Symbol tag;

    public UnionType() {
        tag = null;
    }

    public UnionType(Symbol tag) {
        this.tag = tag;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[UnionType]:" + tag.toString());
    }
}
