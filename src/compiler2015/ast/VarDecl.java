package compiler2015.ast;

public class VarDecl extends Decl {
    public Type type;
    public Symbol name;
    public Initializer init;

    public VarDecl() {
        type = null;
        name = null;
        init = null;
    }

    public VarDecl(Type type, Symbol name, Initializer init) {
        this.type = type;
        this.name = name;
        this.init = init;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[VarDeclaration]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("type:");
    	type.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("name:" + name.toString());
    	if (init != null){
    		for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
        	System.out.println("init:");
        	init.show(indent + 2);
    	}
    }
}
