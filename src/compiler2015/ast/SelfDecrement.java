package compiler2015.ast;

public class SelfDecrement extends Expr {
    public Expr body;

    public SelfDecrement() {
        body = null;
    }

    public SelfDecrement(Expr body) {
        this.body = body;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[SelfDecrement]");
    	body.show(indent + 2);
    }
    
}
