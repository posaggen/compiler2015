package compiler2015.ast;

import java.util.List;

public class UnionDecl extends Decl {
    public Symbol tag;
    public List<VarDecl> fields;

    public UnionDecl() {
        tag = null;
        fields = null;
    }

    public UnionDecl(Symbol tag, List<VarDecl> fields) {
        this.tag = tag;
        this.fields = fields;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[UnionDecl]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("tag:" + tag.toString());
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("fields:");
    	for (VarDecl element : fields){
    		element.show(indent + 2);
    	}
    }
}
