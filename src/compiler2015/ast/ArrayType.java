package compiler2015.ast;

public class ArrayType extends Type {
    public Type baseType;
    public Expr arraySize;
    public int curSize;

    public ArrayType() {
        baseType = null;
        arraySize = null;
    }

    public ArrayType(Type baseType, Expr arraySize) {
        this.baseType = baseType;
        this.arraySize = arraySize;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[ArrayType]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("baseType:");
    	baseType.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("arraySize:");
    	arraySize.show(indent + 2);
    }
}
