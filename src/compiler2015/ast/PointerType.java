package compiler2015.ast;

public class PointerType extends Type {
    public Type baseType;

    public PointerType() {
        baseType = null;
    }

    public PointerType(Type baseType) {
        this.baseType = baseType;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[PointerType]");
    	baseType.show(indent + 2);
    }
    
}
