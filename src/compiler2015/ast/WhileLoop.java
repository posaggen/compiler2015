package compiler2015.ast;

public class WhileLoop extends Stmt {
    public Expr condition;
    public Stmt body;

    public WhileLoop() {
        condition = null;
        body = null;
    }

    public WhileLoop(Expr condition, Stmt body) {
        this.condition = condition;
        this.body = body;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[WhileLoop]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("condition:");
    	condition.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	body.show(indent + 2);
    }
}