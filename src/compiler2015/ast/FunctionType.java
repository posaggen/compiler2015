package compiler2015.ast;

import java.util.List;

public class FunctionType extends Type {
	public Type returnType;
	public Symbol name;
	public List<VarDecl> params;

	public FunctionType() {
		returnType = null;
		name = null;
		params = null;
	}

	public FunctionType(FunctionDecl decl) {
		returnType = decl.returnType;
		name = decl.name;
		params = decl.params;
	}

	public void show(int indent) {
	}

}