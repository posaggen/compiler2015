package compiler2015.ast;

public class StructType extends BasicType {
    public Symbol tag;

    public StructType() {
    }

    public StructType(Symbol tag) {
        this.tag = tag;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[StructType]:" + tag.toString());
    }
}
