package compiler2015.ast;

public class CharConst extends Expr {
    public String value;

    public CharConst() {
    }

    public CharConst(String value) {
        this.value = value;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println(value);
    }
}
