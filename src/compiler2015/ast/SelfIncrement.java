package compiler2015.ast;

public class SelfIncrement extends Expr {
    public Expr body;

    public SelfIncrement() {
        body = null;
    }

    public SelfIncrement(Expr body) {
        this.body = body;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[SelfIncrement]");
    	body.show(indent + 2);
    }
    
}
