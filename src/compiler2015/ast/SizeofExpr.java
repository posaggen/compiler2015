package compiler2015.ast;

public class SizeofExpr extends Expr {
    public Type type;

    public SizeofExpr() {
        type = null;
    }

    public SizeofExpr(Type type) {
        this.type = type;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[SizeofExpression]");
    	type.show(indent + 2);
    }
    
}
