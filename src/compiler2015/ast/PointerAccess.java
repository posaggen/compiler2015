package compiler2015.ast;

public class PointerAccess extends Expr {
    public Expr body;
    public Symbol attribute;

    public PointerAccess() {
        body = null;
        attribute = null;
    }

    public PointerAccess(Expr body, Symbol attribute) {
        this.body = body;
        this.attribute = attribute;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[PointerAccess]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("body:");
    	body.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("attribute:" + attribute.toString());
    }
}
