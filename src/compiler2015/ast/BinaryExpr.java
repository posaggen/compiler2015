package compiler2015.ast;

public class BinaryExpr extends Expr {
    public Expr left;
    public BinaryOp op;
    public Expr right;

    public BinaryExpr() {
        left = null;
        op = null;
        right = null;
    }

    public BinaryExpr(Expr left, BinaryOp op, Expr right) {
        this.left = left;
        this.op = op;
        this.right = right;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[BinaryExpression]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("left:");
    	left.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("BinaryOperation:" + op.toString());
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("right:");
    	right.show(indent + 2);
    }
}
