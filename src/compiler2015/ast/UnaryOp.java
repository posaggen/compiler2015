package compiler2015.ast;

public enum UnaryOp {
    INC, DEC, SIZEOF, AMPERSAND, ASTERISK, PLUS, MINUS, TILDE, NOT
}
