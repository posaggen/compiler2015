package compiler2015.ast;

public class InitValue extends Initializer {
    public Expr expr;

    public InitValue() {
        expr = null;
    }

    public InitValue(Expr expr) {
        this.expr = expr;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[InitValue]");
    	expr.show(indent + 2);
    }
}
