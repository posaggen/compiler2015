package compiler2015.ast;

public class CastExpr extends Expr {
    public Type cast;
    public Expr expr;

    public CastExpr() {
        cast = null;
        expr = null;
    }

    public CastExpr(Type cast, Expr expr) {
        this.cast = cast;
        this.expr = expr;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[CastExpression]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("cast:");
    	cast.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("expr:");
    	expr.show(indent + 2);
    }
}
