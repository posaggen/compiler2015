package compiler2015.ast;

import java.util.List;

public class FunctionCall extends Expr {
    public Symbol body;
    public List<Expr> args;

    public FunctionCall() {
        body = null;
        args = null;
    }

    public FunctionCall(Symbol body, List<Expr> args) {
        this.body = body;
        this.args = args;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[FunctionCall]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println(body.toString());
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("args:");
    	for (Expr element : args){
    		element.show(indent + 2);
    	}
    }
    
}
