package compiler2015.ast;

public abstract class Expr extends Stmt {
	public abstract void show(int indent);
}
