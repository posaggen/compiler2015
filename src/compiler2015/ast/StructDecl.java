package compiler2015.ast;

import java.util.List;
import java.util.LinkedList;

public class StructDecl extends Decl {
    public Symbol tag;
    public List<VarDecl> fields;

    public StructDecl() {
        tag = null;
        fields = null;
    }

    public StructDecl(Symbol tag, List<VarDecl> fields) {
        this.tag = tag;
        this.fields = fields;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[StructDecl]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("tag:" + tag.toString());
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("fields:");
    	for (VarDecl element : fields){
    		element.show(indent + 2);
    	}
    }
    
}
