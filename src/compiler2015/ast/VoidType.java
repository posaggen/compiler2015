package compiler2015.ast;

public class VoidType extends BasicType {
	public void show(int indent){
		for (int i = 0; i < indent; ++i) System.out.print(" ");
		System.out.println("[VoidType]");
	}
}
