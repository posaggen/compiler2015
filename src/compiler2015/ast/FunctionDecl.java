package compiler2015.ast;

import java.util.List;

public class FunctionDecl extends Decl {
    public Type returnType;
    public Symbol name;
    public List<VarDecl> params;
    public CompoundStmt body;

    public FunctionDecl() {
        returnType = null;
        name = null;
        params = null;
        body = null;
    }

    public FunctionDecl(Type returnType, Symbol name, List<VarDecl> params, CompoundStmt body) {
        this.returnType = returnType;
        this.name = name;
        this.params = params;
        this.body = body;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[FunctionDecl]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("returnType:");
    	returnType.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println(name.toString());
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("params:");
    	for (VarDecl element : params){
    		element.show(indent + 2);
    	}
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("body:");
    	body.show(indent + 2);
    }
}
