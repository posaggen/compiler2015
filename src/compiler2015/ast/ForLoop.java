package compiler2015.ast;

public class ForLoop extends Stmt {
    public Expr init;
    public Expr condition;
    public Expr step;
    public Stmt body;

    public ForLoop() {
        init = null;
        condition = null;
        step = null;
        body = null;
    }

    public ForLoop(Expr init, Expr condition, Expr step, Stmt body) {
        this.init = init;
        this.condition = condition;
        this.step = step;
        this.body = body;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[ForLoop]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("init:");
    	init.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("condition:");
    	condition.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("step:");
    	step.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("body:");
    	body.show(indent + 2);
    }
}
