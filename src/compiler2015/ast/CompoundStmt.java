package compiler2015.ast;

import java.util.List;

public class CompoundStmt extends Stmt {
    public List<Decl> decls;
    public List<Stmt> stats;

    public CompoundStmt() {
        decls = null;
        stats = null;
    }

    public CompoundStmt(List<Decl> decls, List<Stmt> stats) {
        this.decls = decls;
        this.stats = stats;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[CompoundStatement]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("decls:");
    	for (Decl element : decls){
    		element.show(indent + 2);
    	}
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("stats:");
    	for (Stmt element : stats){
    		element.show(indent + 2);
    	}
    }
}
