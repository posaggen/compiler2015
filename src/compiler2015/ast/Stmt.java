package compiler2015.ast;

public abstract class Stmt {
	public abstract void show(int indent);
}
