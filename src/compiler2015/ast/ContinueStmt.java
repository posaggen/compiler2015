package compiler2015.ast;

public class ContinueStmt extends Stmt {
	public void show(int indent){
		for (int i = 0; i < indent; ++i) System.out.print(" ");
		System.out.println("[ContinueStatement]");
	}
}
