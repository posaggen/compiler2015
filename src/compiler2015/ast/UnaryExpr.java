package compiler2015.ast;

public class UnaryExpr extends Expr {
    public UnaryOp op;
    public Expr expr;

    public UnaryExpr() {
        op = null;
        expr = null;
    }

    public UnaryExpr(UnaryOp op, Expr expr) {
        this.op = op;
        this.expr = expr;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[UnaryExpression]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("UnaryOp: " + op.toString());
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("expr:");
    	expr.show(indent + 2);
    }
}
