package compiler2015.ast;

import java.util.List;
import java.util.LinkedList;

public class AST {
    public List<Decl> decls;

    public AST() {
        decls = new LinkedList<Decl>();
    }

    public AST(List<Decl> decls) {
        this.decls = decls;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[AST]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("decls:");
    	for (Decl element : decls){
    		element.show(indent + 2);
    	}
    }
}
