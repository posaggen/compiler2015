package compiler2015.ast;

public class RecordAccess extends Expr {
    public Expr body;
    public Symbol attribute;

    public RecordAccess() {
        body = null;
        attribute = null;
    }

    public RecordAccess(Expr body, Symbol attribute) {
        this.body = body;
        this.attribute = attribute;
    }
 
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[RecordAccess]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("body:");
    	body.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("attribute:" + attribute.toString());
    }
    
}
