package compiler2015.ast;

public class ReturnStmt extends Stmt {
    public Expr expr;

    public ReturnStmt() {
        expr = null;
    }

    public ReturnStmt(Expr expr) {
        this.expr = expr;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[ReturnStatement]");
    	expr.show(indent + 2);
    }
}
