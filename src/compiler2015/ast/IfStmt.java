package compiler2015.ast;

public class IfStmt extends Stmt {
    public Expr condition;
    public Stmt consequent;
    public Stmt alternative;

    public IfStmt() {
        condition = null;
        consequent = null;
        alternative = null;
    }

    public IfStmt(Expr condition, Stmt consequent, Stmt alternative) {
        this.condition = condition;
        this.consequent = consequent;
        this.alternative = alternative;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[IfStatment]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("condition:");
    	condition.show(indent + 2);
    	if (consequent != null){
    		for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    		System.out.println("consequent:");
    		consequent.show(indent + 2);
    	}
    	if (alternative != null){
    		for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    		System.out.println("alternative");
    		alternative.show(indent + 2);
    	}
    }
    
}
