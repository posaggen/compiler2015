package compiler2015.ast;

import java.util.List;
import java.util.LinkedList;

public class InitList extends Initializer {
    public List<Initializer> inits;

    public InitList() {
        inits = null;
    }

    public InitList(List<Initializer> inits) {
        this.inits = inits;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[InitList]");
    	for (Initializer element : inits){
    		element.show(indent + 2);
    	}
    }
    
}
