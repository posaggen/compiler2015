package compiler2015.ast;

public class ArrayAccess extends Expr {
    public Expr body;
    public Expr subscript;

    public ArrayAccess() {
        body = null;
        subscript = null;
    }

    public ArrayAccess(Expr body, Expr subscript) {
        this.body = body;
        this.subscript = subscript;
    }
    
    public void show(int indent){
    	for (int i = 0; i < indent; ++i) System.out.print(" ");
    	System.out.println("[ArrayAccess]");
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("body:");
    	body.show(indent + 2);
    	for (int i = 0; i < indent + 1; ++i) System.out.print(" ");
    	System.out.println("subscript:");
    	subscript.show(indent + 2);
    }
    
}
