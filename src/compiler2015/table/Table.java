package compiler2015.table;

import compiler2015.ast.Symbol;

class Binder{
	Object value;
	Symbol pretop;
	Binder tail;
	int level;
	
	Binder (Object _value, Symbol _name, Binder _tail, int _level){
		value = _value;
		pretop = _name;
		tail = _tail;
		level = _level;
	}
}

public class Table{
	
	private java.util.Dictionary<Symbol, Binder> dict = new java.util.Hashtable<Symbol, Binder>();
	private Symbol top = null;
	private Binder last = null;
	private int level = 0;

// add 
	
	public void put(Symbol key, Object obj){
		if (key == null) key = Symbol.get("");
		Binder t = dict.get(key);
		dict.put(key, new Binder(obj, top, t, level));
		top = key;
	}
	
// query
	
	public Object get(Symbol key){
		Binder t = dict.get(key);
		if (t != null) return t.value; else return null;
	}
	
	public int getLevel(Symbol key){
		Binder t = dict.get(key);
		if (t != null) return t.level; else return -1;
	}
	
	public int getLevel(){
		return level;
	}
	
//
	
	public void beginScope(){
		last = new Binder(null, top, last, level);
		top = null;
		++level;
	}
	
//
	
	public void endScope(){
		while (top != null){
			Binder t = dict.get(top);
			if (t.tail == null){
				dict.remove(top);
			} else {
				dict.put(top, t.tail);
			}
			top = t.pretop;
		}
		top = last.pretop;
		last = last.tail;
		--level;
	}
	
}