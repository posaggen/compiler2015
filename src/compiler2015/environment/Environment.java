package compiler2015.environment;

import compiler2015.ast.*;
import compiler2015.table.Table;
import compiler2015.ir.*;

public class Environment {
	public Table typeEnv = null; // for struct & union name
	public Table idenEnv = null; // for identifier & function name
	public Table sizeEnv = null; // for size
	public Table addrEnv = null; // for address

	public Environment() {
		initTypeEnv();
		initIdenEnv();
		initSizeEnv();
		initAddrEnv();
	}

	public void initAddrEnv(){
		addrEnv = new Table();
	}
	
	public void putAddr(Symbol name, Object obj){
		addrEnv.put(name, obj);
	}
	
	public Address getAddress(Symbol name){
		return (Address)addrEnv.get(name);
	}
	
	public void initTypeEnv() {
		typeEnv = new Table();
	}

	public void initIdenEnv() {
		idenEnv = new Table();
		idenEnv.put(Symbol.get("printf"), new FunctionDecl(new IntType(),
				Symbol.get("printf"), null, null));
		idenEnv.put(Symbol.get("malloc"), new FunctionDecl(new PointerType(
				new VoidType()), Symbol.get("malloc"), null, null));
		idenEnv.put(Symbol.get("getchar"), new FunctionDecl(new IntType(),
				Symbol.get("getchar"), null, null));
	}
	
	public void initSizeEnv(){
		sizeEnv = new Table();
	}
	
	public void putSize(Symbol name, Object type) {
		sizeEnv.put(name, type);
	}
	
	public int getSize(Symbol name){
		return (int)sizeEnv.get(name);
	}
	
	public void beginScope() {
		typeEnv.beginScope();
		idenEnv.beginScope();
		sizeEnv.beginScope();
		addrEnv.beginScope();
	}

	public void endScope() {
		typeEnv.endScope();
		idenEnv.endScope();
		sizeEnv.endScope();
		addrEnv.endScope();
	}

	public void putType(Symbol name, Object type) {
		typeEnv.put(name, type);
	}

	public void putIden(Symbol name, Object type) {
		idenEnv.put(name, type);
	}
	
	public Object getIden(Symbol name){
		return idenEnv.get(name);
	}
	
	public int getIdenLevel(Symbol name){
		return idenEnv.getLevel(name);
	}
	
	public Object getType(Symbol name){
		return typeEnv.get(name);
	}
	
	public int getTypeLevel(Symbol name){
		return typeEnv.getLevel(name);
	}
	
	public int getLevel(){
		return typeEnv.getLevel();
	}

}
