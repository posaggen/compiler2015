package compiler2015.ir;

public class BasicParam extends Param {
    public Address src;
    public boolean isSpecial;

    public void show(){
    	System.out.println("Param " + src.show());
    }
    
    public BasicParam() {
        src = null;
    }

    public BasicParam(Address src, boolean _isSpecial) {
        this.src = src;
        this.isSpecial = _isSpecial;
    }
}
