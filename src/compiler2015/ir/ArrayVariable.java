package compiler2015.ir;

public class ArrayVariable extends Variable {
	
	public ArrayVariable() {
        addr = null;
        size = 0;
    }

    public ArrayVariable(Address addr, int size, boolean b) {
        this.addr = addr;
        this.size = size;
        this.isSpecial = b;
    }
}
