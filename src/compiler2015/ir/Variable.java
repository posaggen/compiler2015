package compiler2015.ir;

public abstract class Variable {
    public Address addr;
    public int size;
    public boolean isSpecial;
}
