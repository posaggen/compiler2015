package compiler2015.ir;

import java.util.List;
import java.util.LinkedList;

public class IR {
    public List<Function> fragments;
    
    public void show(){
    	for (Function element : fragments){
    		element.show();
    	}
    }

    public IR() {
        fragments = new LinkedList<Function>();
    }

    public IR(List<Function> fragments) {
        this.fragments = fragments;
    }
}
