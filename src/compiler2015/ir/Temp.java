package compiler2015.ir;

public class Temp extends Address {
    private static int tempCount = 0;

    public int num;
    
    public String show(){
       	return new String("t" + num);
    }

    public Temp() {
        num = tempCount++;
    }
}
