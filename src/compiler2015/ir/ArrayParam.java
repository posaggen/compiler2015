package compiler2015.ir;

public class ArrayParam extends Param {
    public Address name;
    public Address offset;
    public int size;
    
    public void show(){
    	System.out.println("Param " + name.show());
    }

    public ArrayParam() {
        name = null;
        offset = null;
        size = 0;
    }

    public ArrayParam(Address name, Address offset, int size) {
        this.name = name;
        this.offset = offset;
        this.size = size;
    }
}
