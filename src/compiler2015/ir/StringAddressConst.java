package compiler2015.ir;

public class StringAddressConst extends Const {
    public String value;
    
    public String show(){
    	return value;
    }

    public StringAddressConst() {
        value = null;
    }

    public StringAddressConst(String value) {
        this.value = value;
    }
}
