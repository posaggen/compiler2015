package compiler2015.ir;

public class Return extends Quadruple {
    public Param value;

    public void show(){
    	System.out.println("Return");
    }
    
    public Return() {
        value = null;
    }

    public Return(Param value) {
        this.value = value;
    }
}
