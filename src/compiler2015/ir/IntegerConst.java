package compiler2015.ir;

public class IntegerConst extends Const {
    public int value;

    public String show(){
    	return "" + value;
    }
    
    public IntegerConst() {
    }

    public IntegerConst(int value) {
        this.value = value;
    }
}
