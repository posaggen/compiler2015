package compiler2015.ir;

public class Goto extends Quadruple {
    public Label label;

    public void show(){
    	System.out.print("goto ");
    	label.show();
    }
    
    public Goto() {
        label = null;
    }

    public Goto(Label label) {
        this.label = label;
    }
}
