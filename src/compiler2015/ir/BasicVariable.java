package compiler2015.ir;

public class BasicVariable extends Variable {
    public BasicVariable() {
        addr = null;
        size = 0;
    }

    public BasicVariable(Address addr, int size, boolean b) {
        this.addr = addr;
        this.size = size;
        this.isSpecial = b;
    }
}
