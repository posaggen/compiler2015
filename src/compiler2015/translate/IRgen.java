package compiler2015.translate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import compiler2015.ast.*;
import compiler2015.environment.*;
import compiler2015.util.*;
import compiler2015.ir.*;
import compiler2015.ast.*;

public class IRgen {

	private Environment env;
	private Label breakLabel;
	private Label continueLabel;
	private Label returnLabel;
	private Function __start;
	private IntegerConst ZERO;
	private IntegerConst ONE;
	private IntegerConst FOUR;
	private java.util.Dictionary<Symbol, Address> opt = new java.util.Hashtable<Symbol, Address>();

	int fix(int x) {
		x += (4 - x % 4) % 4;
		return x;
	}

	private Type arrayToPointer(Type returnType) {
		while (returnType instanceof ArrayType) {
			returnType = ((ArrayType) returnType).baseType;
		}
		returnType = new PointerType(returnType);
		return returnType;
	}

	private int getSize(Type t) {
		if (t instanceof ArrayType) {
			int ret = ((ArrayType) t).curSize;
			ret *= getSize(((ArrayType) t).baseType);
			return ret;
		}
		if (t instanceof IntType || t instanceof PointerType
				|| t instanceof CharType) {
			return 4;
		} else if (t instanceof CharType || t instanceof VoidType) {
			return 4;
		} else if (t instanceof StructType) {
			return env.getSize(((StructType) t).tag);
		} else if (t instanceof UnionType) {
			return env.getSize(((UnionType) t).tag);
		}
		return 4;
	}

	private Type getBaseType(Type t) {
		Type returnType = t;
		while (returnType instanceof ArrayType) {
			returnType = ((ArrayType) returnType).baseType;
		}
		return returnType;
	}

	public IRgen() {
		env = new Environment();
		ZERO = new IntegerConst(0);
		ONE = new IntegerConst(1);
		FOUR = new IntegerConst(4);
	}

	public IR genAST(AST ast) {
		IR ir = new IR();
		Function _start = new Function();
		_start.name = "__start";
		__start = _start;
		for (Decl element : ast.decls) {
			if (element instanceof FunctionDecl)
				ir.fragments.add(genFunctionDecl((FunctionDecl) element));
			else if (element instanceof VarDecl)
				genVarDecl((VarDecl) element, _start);
			else if (element instanceof StructDecl)
				genStructDecl((StructDecl) element, _start);
			else if (element instanceof UnionDecl) {
				genUnionDecl((UnionDecl) element, _start);
			}
		}
		ir.fragments.add(_start);
		return ir;
	}

	private Function genFunctionDecl(FunctionDecl decl) {

		Function func = new Function();
		func.name = decl.name.toString();
		func.size = getSize(decl.returnType);
		returnLabel = new Label();

		// add

		env.putIden(decl.name, decl);

		// params & body judge

		env.beginScope();

		for (VarDecl element : decl.params) {
			Symbol name = element.name;
			if (element.type instanceof ArrayType) {
				ArrayType tt = (ArrayType) element.type;
				while (true) {
					tt.curSize = ((IntegerConst) (genExpr(tt.arraySize, func,
							false).addr)).value;
					if (tt.baseType instanceof ArrayType) {
						tt = (ArrayType) tt.baseType;
					} else
						break;
				}
			}
			env.putIden(name, element.type);
			Name nn = new Name(name.toString());
			env.putAddr(name, nn);
			Type t = element.type;
			if (t instanceof StructType || t instanceof UnionType) {
				func.args.add(new ArrayVariable(nn, getSize(t), false));
			} else if (t instanceof ArrayType) {
				func.args.add(new BasicVariable(nn, 4, true));
			} else {
				func.args.add(new BasicVariable(nn, getSize(t), false));
			}
		}

		genCompoundStmt(decl.body, decl.returnType, func);

		env.endScope();

		return func;
	}

	private void genStructDecl(StructDecl decl, Function func) {

		// redeclaration judge

		Symbol name = decl.tag;

		// fields judge

		env.beginScope();

		int sz = 0;
		for (VarDecl element : decl.fields) {
			env.putIden(element.name, element.type);
			if (element.type instanceof ArrayType) {
				ArrayType tt = (ArrayType) element.type;
				while (true) {
					tt.curSize = ((IntegerConst) (genExpr(tt.arraySize, func,
							false).addr)).value;
					if (tt.baseType instanceof ArrayType) {
						tt = (ArrayType) tt.baseType;
					} else
						break;
				}
			}
			sz += fix(getSize(element.type));
		}

		env.endScope();
		env.putSize(name, sz);

		// add

		env.putType(name, decl);

	}

	private void genUnionDecl(UnionDecl decl, Function func) {

		// redeclaration judge

		Symbol name = decl.tag;

		// fields judge

		env.beginScope();

		int sz = 0;

		for (VarDecl element : decl.fields) {
			env.putIden(element.name, element.type);
			if (element.type instanceof ArrayType) {
				ArrayType tt = (ArrayType) element.type;
				while (true) {
					tt.curSize = ((IntegerConst) (genExpr(tt.arraySize, func,
							false).addr)).value;
					if (tt.baseType instanceof ArrayType) {
						tt = (ArrayType) tt.baseType;
					} else
						break;
				}
			}
			if (sz < fix(getSize(element.type)))
				sz = fix(getSize(element.type));
		}

		env.endScope();
		sz += (4 - sz % 4) % 4;
		env.putSize(name, sz);

		// add

		env.putType(name, decl);

	}

	private void genVarDecl(VarDecl decl, Function func) {

		Symbol name = decl.name;

		// add

		if (decl.type instanceof ArrayType) {
			ArrayType tt = (ArrayType) decl.type;
			while (true) {
				tt.curSize = ((IntegerConst) (genExpr(tt.arraySize, func, false).addr)).value;
				if (tt.baseType instanceof ArrayType) {
					tt = (ArrayType) tt.baseType;
				} else
					break;
			}
		}

		env.putIden(name, decl.type);
		Name nn = new Name(name.toString());
		env.putAddr(name, nn);

		Type t = decl.type;
		if (t instanceof StructType || t instanceof UnionType
				|| t instanceof ArrayType) {
			func.vars.add(new ArrayVariable(nn, getSize(t), false));
		} else {
			func.vars.add(new BasicVariable(nn, getSize(t), false));
		}

		// initializer judge

		if (decl.init != null) {
			Temp tmp = new Temp();
			func.vars.add(new BasicVariable(tmp, 4, false));
			func.body.add(new AddressOf(tmp, nn));
			genInitializer(decl.init, getSize(decl.type), 0, func, tmp,
					decl.type);
		}

	}

	private void genInitializer(Initializer init, int Size, int curSize,
			Function func, Address addr, Type t) {
		if (curSize == Size)
			return;
		if (init instanceof InitValue) {
			genInitValue((InitValue) init, Size, curSize, func, addr, t);
		} else if (init instanceof InitList) {
			genInitList((InitList) init, Size, curSize, func, addr, t);
		}
	}

	private void genInitValue(InitValue init, int Size, int curSize,
			Function func, Address addr, Type tl) {
		// System.out.println("Value :" + curSize);
		All r = genExpr(init.expr, func, false);
		if (tl instanceof StructType || tl instanceof UnionType
				|| (tl instanceof ArrayType && r.type instanceof ArrayType)) {
			int sz = getSize(tl);
			Temp t = new Temp(), posL = new Temp(), posR = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			func.vars.add(new BasicVariable(posL, 4, false));
			func.vars.add(new BasicVariable(posR, 4, false));
			func.body.add(new Assign(posL, addr));
			func.body.add(new ArithmeticExpr(posL, posL, ArithmeticOp.ADD,
					new IntegerConst(curSize)));
			func.body.add(new Assign(posR, r.addr));
			for (IntegerConst i = new IntegerConst(0); i.value < sz; i.value += 4) {
				MemoryRead mr = new MemoryRead(t, posR, 4);
				func.body.add(mr);
				MemoryWrite mw = new MemoryWrite(posL, t, 4);
				func.body.add(mw);
				func.body.add(new ArithmeticExpr(posL, posL, ArithmeticOp.ADD,
						FOUR));
				func.body.add(new ArithmeticExpr(posR, posR, ArithmeticOp.ADD,
						FOUR));
			}
		} else {
			if (curSize > 0) {
				Temp t = new Temp();
				func.vars.add(new BasicVariable(t, 4, false));
				func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.ADD,
						new IntegerConst(curSize)));
				func.body.add(new MemoryWrite(t, r.addr, getSize(tl)));
			} else {
				func.body.add(new MemoryWrite(addr, r.addr, getSize(tl)));
			}
		}
	}

	private void genInitList(InitList init, int sz, int curSize, Function func,
			Address addr, Type t) {
		// System.out.println("List :" + curSize);
		ArrayType tt = (ArrayType) t;
		for (Initializer element : init.inits) {
			if (element instanceof InitValue) {
				genInitValue((InitValue) element, sz, curSize, func, addr,
						tt.baseType);
				curSize += getSize(tt.baseType);
			} else {
				genInitList((InitList) element, sz, curSize, func, addr,
						tt.baseType);
				curSize += getSize(tt.baseType);
			}
		}
	}

	private void genStmt(Stmt stmt, Type type, Function func) {
		if (stmt instanceof BreakStmt) {
			genBreakStmt((BreakStmt) stmt, type, func);
		} else if (stmt instanceof ContinueStmt) {
			genContinueStmt((ContinueStmt) stmt, type, func);
		} else if (stmt instanceof IfStmt) {
			genIfStmt((IfStmt) stmt, type, func);
		} else if (stmt instanceof ForLoop) {
			genForLoop((ForLoop) stmt, type, func);
		} else if (stmt instanceof WhileLoop) {
			genWhileLoop((WhileLoop) stmt, type, func);
		} else if (stmt instanceof ReturnStmt) {
			genReturnStmt((ReturnStmt) stmt, type, func);
		} else if (stmt instanceof CompoundStmt) {
			genCompoundStmt((CompoundStmt) stmt, type, func);
		} else if (stmt instanceof Expr) {
			genExpr((Expr) stmt, func, false);
		}
	}

	private void genBreakStmt(BreakStmt stmt, Type type, Function func) {
		func.body.add(new Goto(breakLabel));
	}

	private void genContinueStmt(ContinueStmt stmt, Type type, Function func) {
		func.body.add(new Goto(continueLabel));
	}

	private void genIfStmt(IfStmt stmt, Type type, Function func) {
		Label lb1 = new Label();
		Label lb2 = new Label();
		All all = genExpr(stmt.condition, func, false);
		IfTrueGoto tmp = new IfTrueGoto(all.addr, RelationalOp.EQ, ZERO, lb1);
		func.body.add(tmp);

		genStmt(stmt.consequent, type, func);

		func.body.add(new Goto(lb2));
		func.body.add(lb1);
		if (stmt.alternative != null) {
			genStmt(stmt.alternative, type, func);
		}
		func.body.add(lb2);
	}

	private void genForLoop(ForLoop flp, Type type, Function func) {
		Label lb1 = new Label();
		Label lb2 = new Label();
		Label lb3 = new Label();
		Label preBreakLabel = breakLabel;
		Label preContinueLabel = continueLabel;
		breakLabel = lb1;
		continueLabel = lb3;

		genExpr(flp.init, func, false);
		func.body.add(lb2);
		All all = genExpr(flp.condition, func, false);
		IfTrueGoto tmp = new IfTrueGoto(all.addr, RelationalOp.EQ, ZERO, lb1);
		func.body.add(tmp);

		genStmt(flp.body, type, func);
		func.body.add(lb3);
		genExpr(flp.step, func, false);
		func.body.add(new Goto(lb2));
		func.body.add(lb1);

		breakLabel = preBreakLabel;
		continueLabel = preContinueLabel;
	}

	private void genWhileLoop(WhileLoop wlp, Type type, Function func) {
		Label lb1 = new Label();
		Label lb2 = new Label();
		Label preBreakLabel = breakLabel;
		Label preContinueLabel = continueLabel;
		breakLabel = lb1;
		continueLabel = lb2;

		func.body.add(lb2);
		All all = genExpr(wlp.condition, func, false);
		IfTrueGoto tmp = new IfTrueGoto(all.addr, RelationalOp.EQ, ZERO, lb1);
		func.body.add(tmp);

		genStmt(wlp.body, type, func);
		func.body.add(new Goto(lb2));
		func.body.add(lb1);

		breakLabel = preBreakLabel;
		continueLabel = preContinueLabel;
	}

	private void genReturnStmt(ReturnStmt stmt, Type type, Function func) {
		All t = genExpr(stmt.expr, func, false);
		// return;
		if (t.type == null) {
			func.body.add(new Return());
		} else if (t.type instanceof ArrayType) {
			Temp tmp = new Temp();
			func.vars.add(new BasicVariable(tmp, 4, false));
			if (t.addr instanceof Name) {
				func.body.add(new AddressOf(tmp, (Name) t.addr));
			} else {
				func.body.add(new Assign(tmp, t.addr));
			}
			func.body.add(new Return(new BasicParam(tmp, true)));
		} else if (t.type instanceof StructType || t.type instanceof UnionType) {
			func.body.add(new Return(new ArrayParam(t.addr, ZERO,
					getSize(t.type))));
		} else {
			func.body.add(new Return(new BasicParam(t.addr, false)));
		}

	}

	private void genCompoundStmt(CompoundStmt stmt, Type type, Function func) {
		env.beginScope();

		for (Decl element : stmt.decls) {
			genVarDecl((VarDecl) element, func);
		}

		for (Stmt element : stmt.stats) {
			genStmt(element, type, func);
		}

		env.endScope();
	}

	private All genExpr(Expr expr, Function func, boolean isLeft) {
		if (expr instanceof EmptyExpr) {
			return genEmptyExpr((EmptyExpr) expr, func);
		} else if (expr instanceof BinaryExpr) {
			return genBinaryExpr((BinaryExpr) expr, func);
		} else if (expr instanceof UnaryExpr) {
			return genUnaryExpr((UnaryExpr) expr, func, isLeft);
		} else if (expr instanceof SizeofExpr) {
			return genSizeofExpr((SizeofExpr) expr, func);
		} else if (expr instanceof CastExpr) {
			return genCastExpr((CastExpr) expr, func);
		} else if (expr instanceof PointerAccess) {
			return genPointerAccess((PointerAccess) expr, func, isLeft);
		} else if (expr instanceof RecordAccess) {
			return genRecordAccess((RecordAccess) expr, func, isLeft);
		} else if (expr instanceof SelfIncrement) {
			return genSelfIncrement((SelfIncrement) expr, func);
		} else if (expr instanceof SelfDecrement) {
			return genSelfDecrement((SelfDecrement) expr, func);
		} else if (expr instanceof ArrayAccess) {
			return genArrayAccess((ArrayAccess) expr, func, isLeft);
		} else if (expr instanceof FunctionCall) {
			return genFunctionCall((FunctionCall) expr, func);
		} else if (expr instanceof Identifier) {
			return genIdentifier((Identifier) expr, func, isLeft);
		} else if (expr instanceof IntConst) {
			return genIntConst((IntConst) expr);
		} else if (expr instanceof CharConst) {
			return genCharConst((CharConst) expr);
		} else {
			return genStringConst((StringConst) expr, func);
		}
	}

	private All genEmptyExpr(EmptyExpr expr, Function func) {
		return new All(null, ONE);
	}

	private All genBinaryExpr(BinaryExpr expr, Function func) {

		// ,

		if (expr.op == BinaryOp.COMMA) {
			genExpr(expr.left, func, false);
			All r = genExpr(expr.right, func, false);
			return r;
		}

		// =

		if (expr.op == BinaryOp.ASSIGN) {
			All r = genExpr(expr.right, func, false);
			All l = genExpr(expr.left, func, true);
			Type tl = l.type;
			if (tl instanceof StructType || tl instanceof UnionType) {
				int sz = getSize(tl);
				Temp t = new Temp(), posL = new Temp(), posR = new Temp();
				func.vars.add(new BasicVariable(t, 4, false));
				func.vars.add(new BasicVariable(posL, 4, false));
				func.vars.add(new BasicVariable(posR, 4, false));
				func.body.add(new Assign(posL, l.addr));
				func.body.add(new Assign(posR, r.addr));
				for (IntegerConst i = new IntegerConst(0); i.value < sz; i.value += 4) {
					MemoryRead mr = new MemoryRead(t, posR, 4);
					func.body.add(mr);
					MemoryWrite mw = new MemoryWrite(posL, t, 4);
					func.body.add(mw);
					func.body.add(new ArithmeticExpr(posL, posL,
							ArithmeticOp.ADD, FOUR));
					func.body.add(new ArithmeticExpr(posR, posR,
							ArithmeticOp.ADD, FOUR));
				}
			} else {
				func.body.add(new MemoryWrite(l.addr, r.addr, getSize(tl)));
			}
			return r;
		}

		// *= /= %= <<= >>= ^= &= |=

		if (expr.op == BinaryOp.ASSIGN_MUL || expr.op == BinaryOp.ASSIGN_DIV
				|| expr.op == BinaryOp.ASSIGN_MOD
				|| expr.op == BinaryOp.ASSIGN_SHL
				|| expr.op == BinaryOp.ASSIGN_SHR
				|| expr.op == BinaryOp.ASSIGN_AND
				|| expr.op == BinaryOp.ASSIGN_XOR
				|| expr.op == BinaryOp.ASSIGN_OR
				|| expr.op == BinaryOp.ASSIGN_ADD
				|| expr.op == BinaryOp.ASSIGN_SUB) {
			BinaryOp op;
			if (expr.op == BinaryOp.ASSIGN_MUL)
				op = BinaryOp.MUL;
			else if (expr.op == BinaryOp.ASSIGN_DIV)
				op = BinaryOp.DIV;
			else if (expr.op == BinaryOp.ASSIGN_MOD)
				op = BinaryOp.MOD;
			else if (expr.op == BinaryOp.ASSIGN_SHL)
				op = BinaryOp.SHL;
			else if (expr.op == BinaryOp.ASSIGN_SHR)
				op = BinaryOp.SHR;
			else if (expr.op == BinaryOp.ASSIGN_AND)
				op = BinaryOp.AND;
			else if (expr.op == BinaryOp.ASSIGN_XOR)
				op = BinaryOp.XOR;
			else if (expr.op == BinaryOp.ASSIGN_OR)
				op = BinaryOp.OR;
			else if (expr.op == BinaryOp.ASSIGN_ADD)
				op = BinaryOp.ADD;
			else
				op = BinaryOp.SUB;
			BinaryExpr be = new BinaryExpr(expr.left, op, expr.right);
			be = new BinaryExpr(expr.left, BinaryOp.ASSIGN, be);
			genBinaryExpr(be, func);
		}

		// ||

		if (expr.op == BinaryOp.LOGICAL_OR) {
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			Label lb1 = new Label();
			Label lb2 = new Label();
			func.body.add(new Assign(t, ZERO));
			All all = genExpr(expr.left, func, false);
			func.body.add(new IfTrueGoto(all.addr, RelationalOp.NE, ZERO, lb1));
			all = genExpr(expr.right, func, false);
			func.body.add(new IfTrueGoto(all.addr, RelationalOp.EQ, ZERO, lb2));
			func.body.add(lb1);
			func.body.add(new Assign(t, ONE));
			func.body.add(lb2);
			return new All(new IntType(), t);
		}

		// &&
		if (expr.op == BinaryOp.LOGICAL_AND) {
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			Label lb = new Label();
			func.body.add(new Assign(t, ZERO));
			All all = genExpr(expr.left, func, false);
			func.body.add(new IfTrueGoto(all.addr, RelationalOp.EQ, ZERO, lb));
			all = genExpr(expr.right, func, false);
			func.body.add(new IfTrueGoto(all.addr, RelationalOp.EQ, ZERO, lb));
			func.body.add(new Assign(t, ONE));
			func.body.add(lb);
			return new All(new IntType(), t);
		}

		// == != >= <= > <

		if (expr.op == BinaryOp.EQ || expr.op == BinaryOp.NE
				|| expr.op == BinaryOp.LT || expr.op == BinaryOp.GT
				|| expr.op == BinaryOp.LE || expr.op == BinaryOp.GE) {
			RelationalOp op;
			if (expr.op == BinaryOp.EQ)
				op = RelationalOp.EQ;
			else if (expr.op == BinaryOp.NE)
				op = RelationalOp.NE;
			else if (expr.op == BinaryOp.LT)
				op = RelationalOp.LT;
			else if (expr.op == BinaryOp.GT)
				op = RelationalOp.GT;
			else if (expr.op == BinaryOp.LE)
				op = RelationalOp.LE;
			else
				op = RelationalOp.GE;
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			All l = genExpr(expr.left, func, false);
			All r = genExpr(expr.right, func, false);
			Label lb = new Label();
			func.body.add(new Assign(t, ONE));
			func.body.add(new IfTrueGoto(l.addr, op, r.addr, lb));
			func.body.add(new Assign(t, ZERO));
			func.body.add(lb);
			return new All(new IntType(), t);
		}

		// | ^ & << >> * / %

		if (expr.op == BinaryOp.OR || expr.op == BinaryOp.AND
				|| expr.op == BinaryOp.XOR || expr.op == BinaryOp.SHL
				|| expr.op == BinaryOp.SHR || expr.op == BinaryOp.MUL
				|| expr.op == BinaryOp.DIV || expr.op == BinaryOp.MOD) {
			All l = genExpr(expr.left, func, false);
			All r = genExpr(expr.right, func, false);
			// IntType tl = (IntType) l.type;
			// IntType tr = (IntType) r.type;
			ArithmeticOp op;
			IntType returnType = new IntType();
			if (expr.op == BinaryOp.OR) {
				op = ArithmeticOp.OR;
				// returnType.value = tl.value | tr.value;
			} else if (expr.op == BinaryOp.AND) {
				op = ArithmeticOp.AND;
				// returnType.value = tl.value & tr.value;
			} else if (expr.op == BinaryOp.XOR) {
				op = ArithmeticOp.XOR;
				// returnType.value = tl.value ^ tr.value;
			} else if (expr.op == BinaryOp.SHL) {
				op = ArithmeticOp.SHL;
				// returnType.value = tl.value << tr.value;
			} else if (expr.op == BinaryOp.SHR) {
				op = ArithmeticOp.SHR;
				// returnType.value = tl.value >> tr.value;
			} else if (expr.op == BinaryOp.MUL) {
				op = ArithmeticOp.MUL;
				// returnType.value = tl.value * tr.value;
			} else if (expr.op == BinaryOp.DIV) {
				op = ArithmeticOp.DIV;
				// if (tr.value != 0)
				// returnType.value = tl.value / tr.value;
			} else {
				op = ArithmeticOp.MOD;
				// if (tr.value != 0)
				// returnType.value = tl.value % tr.value;
			}
			if (l.addr instanceof IntegerConst
					&& r.addr instanceof IntegerConst) {
				int ll = ((IntegerConst) l.addr).value;
				int rr = ((IntegerConst) r.addr).value;
				if (expr.op == BinaryOp.OR) {
					return new All(new IntType(), new IntegerConst(ll | rr));
				} else if (expr.op == BinaryOp.AND) {
					return new All(new IntType(), new IntegerConst(ll & rr));
				} else if (expr.op == BinaryOp.XOR) {
					return new All(new IntType(), new IntegerConst(ll ^ rr));
				} else if (expr.op == BinaryOp.SHL) {
					return new All(new IntType(), new IntegerConst(ll << rr));
				} else if (expr.op == BinaryOp.SHR) {
					return new All(new IntType(), new IntegerConst(ll | rr));
				} else if (expr.op == BinaryOp.MUL) {
					return new All(new IntType(), new IntegerConst(ll * rr));
				} else if (expr.op == BinaryOp.DIV) {
					return new All(new IntType(), new IntegerConst(ll / rr));
				} else {
					return new All(new IntType(), new IntegerConst(ll % rr));
				}
			}
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			func.body.add(new ArithmeticExpr(t, l.addr, op, r.addr));
			return new All(returnType, t);
		}

		// +

		if (expr.op == BinaryOp.ADD) {
			All l = genExpr(expr.left, func, false);
			All r = genExpr(expr.right, func, false);
			Type tl = l.type;
			Type tr = r.type;
			if (l.addr instanceof IntegerConst
					&& r.addr instanceof IntegerConst) {
				int ll = ((IntegerConst) l.addr).value;
				int rr = ((IntegerConst) r.addr).value;
				return new All(new IntType(), new IntegerConst(ll + rr));
			}
			Type returnType;
			Address addl = l.addr;
			Address addr = r.addr;
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			if (tl instanceof PointerType) {
				int step = getSize(((PointerType) tl).baseType);
				func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.MUL,
						new IntegerConst(step)));
				func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.ADD, t));
				returnType = tl;
			} else if (tl instanceof ArrayType) {
				int step = getSize(getBaseType(tl));
				func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.MUL,
						new IntegerConst(step)));
				func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.ADD, t));
				returnType = new PointerType(getBaseType(tl));
			} else if (tr instanceof PointerType) {
				int step = getSize(((PointerType) tr).baseType);
				func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.MUL,
						new IntegerConst(step)));
				func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.ADD, t));
				returnType = tr;
			} else if (tr instanceof ArrayType) {
				int step = getSize(getBaseType(tr));
				func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.MUL,
						new IntegerConst(step)));
				func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.ADD, t));
				returnType = new PointerType(getBaseType(tr));
			} else {
				func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.ADD,
						addr));
				returnType = new IntType();
				if (tl instanceof IntType && tr instanceof IntType) {
					int t1 = ((IntType) tl).value;
					int t2 = ((IntType) tr).value;
					((IntType) returnType).value = t1 + t2;
				}

			}
			return new All(returnType, t);
		}

		// -

		if (expr.op == BinaryOp.SUB) {
			All l = genExpr(expr.left, func, false);
			All r = genExpr(expr.right, func, false);
			Type tl = l.type;
			Type tr = r.type;
			if (l.addr instanceof IntegerConst
					&& r.addr instanceof IntegerConst) {
				int ll = ((IntegerConst) l.addr).value;
				int rr = ((IntegerConst) r.addr).value;
				return new All(new IntType(), new IntegerConst(ll - rr));
			}
			Type returnType;
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			Address addl = l.addr;
			Address addr = r.addr;
			if (tl instanceof PointerType) {
				int step = getSize(((PointerType) tl).baseType);
				if (tr instanceof PointerType || tr instanceof ArrayType) {
					func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.SUB,
							addr));
					func.body.add(new ArithmeticExpr(t, t, ArithmeticOp.DIV,
							new IntegerConst(step)));
					returnType = new IntType();
				} else {
					func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.MUL,
							new IntegerConst(step)));
					func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.SUB,
							t));
					returnType = tl;
				}
			} else if (tl instanceof ArrayType) {
				int step = getSize(getBaseType(tl));
				if (tr instanceof PointerType || tr instanceof ArrayType) {
					func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.SUB,
							addr));
					func.body.add(new ArithmeticExpr(t, t, ArithmeticOp.DIV,
							new IntegerConst(step)));
					returnType = new IntType();
				} else {
					func.body.add(new ArithmeticExpr(t, addr, ArithmeticOp.MUL,
							new IntegerConst(step)));
					func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.SUB,
							t));
					returnType = tl;
				}
			} else {
				func.body.add(new ArithmeticExpr(t, addl, ArithmeticOp.SUB,
						addr));
				returnType = new IntType();
				if (tl instanceof IntType && tr instanceof IntType) {
					int t1 = ((IntType) tl).value;
					int t2 = ((IntType) tr).value;
					((IntType) returnType).value = t1 - t2;
				}
			}
			return new All(returnType, t);
		}

		return null;
	}

	private All genUnaryExpr(UnaryExpr expr, Function func, boolean isLeft) {

		// -- ++

		if (expr.op == UnaryOp.INC || expr.op == UnaryOp.DEC) {
			BinaryOp op;
			if (expr.op == UnaryOp.INC)
				op = BinaryOp.ADD;
			else
				op = BinaryOp.SUB;
			BinaryExpr be = new BinaryExpr(expr.expr, op, new IntConst(1));
			be = new BinaryExpr(expr.expr, BinaryOp.ASSIGN, be);
			return genBinaryExpr(be, func);
		}

		// sizeof

		if (expr.op == UnaryOp.SIZEOF) {
			All all = genExpr(expr.expr, func, false);
			return new All(new IntType(), new IntegerConst(getSize(all.type)));
		}

		// &

		if (expr.op == UnaryOp.AMPERSAND) {
			All all = genExpr(expr.expr, func, true);
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			if (all.addr instanceof Name) {
				func.body.add(new AddressOf(t, (Name) (all.addr)));
			} else {
				func.body.add(new Assign(t, all.addr));
			}
			return new All(new PointerType(all.type), t);
		}

		// *

		if (expr.op == UnaryOp.ASTERISK) {
			All all = genExpr(expr.expr, func, false);
			Type returnType = all.type;
			if (returnType instanceof ArrayType)
				returnType = arrayToPointer(returnType);
			returnType = ((PointerType) returnType).baseType;
			if (isLeft) {
				All ret = new All(returnType, all.addr);
				return ret;
			} else {
				if (returnType instanceof StructType
						|| returnType instanceof UnionType) {
					All ret = new All(returnType, all.addr);
					return ret;
				} else {
					Temp t = new Temp();
					func.vars.add(new BasicVariable(t, 4, false));
					func.body.add(new MemoryRead(t, all.addr,
							getSize(returnType)));
					All ret = new All(returnType, t);
					return ret;
				}
			}
		}

		// + - ~

		if (expr.op == UnaryOp.PLUS) {
			All all = genExpr(expr.expr, func, false);
			return new All(all.type, all.addr);
		}

		if (expr.op == UnaryOp.MINUS) {
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			All all = genExpr(expr.expr, func, false);
			func.body.add(new ArithmeticExpr(t, ArithmeticOp.MINUS, all.addr));
			return new All(all.type, t);
		}

		if (expr.op == UnaryOp.TILDE) {
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			All all = genExpr(expr.expr, func, false);
			func.body.add(new ArithmeticExpr(t, ArithmeticOp.TILDE, all.addr));
			return new All(all.type, t);
		}

		// !

		if (expr.op == UnaryOp.NOT) {
			All all = genExpr(expr.expr, func, false);
			Label lb = new Label();
			Temp t = new Temp();
			func.vars.add(new BasicVariable(t, 4, false));
			func.body.add(new Assign(t, ZERO));
			func.body.add(new IfTrueGoto(all.addr, RelationalOp.NE, ZERO, lb));
			func.body.add(new Assign(t, ONE));
			func.body.add(lb);
			return new All(new IntType(), t);
		}

		return null;
	}

	private All genSizeofExpr(SizeofExpr expr, Function func) {
		return new All(new IntType(), new IntegerConst(getSize(expr.type)));
	}

	private All genCastExpr(CastExpr expr, Function func) {
		All all = genExpr(expr.expr, func, false);
		Temp t = new Temp();
		func.vars.add(new BasicVariable(t, 4, false));
		func.body.add(new Assign(t, all.addr));
		return new All(expr.cast, t);
	}

	private All genPointerAccess(PointerAccess expr, Function func,
			boolean isLeft) {
		UnaryExpr ue = new UnaryExpr(UnaryOp.ASTERISK, expr.body);
		return genExpr(new RecordAccess(ue, expr.attribute), func, isLeft);
	}

	private All genRecordAccess(RecordAccess expr, Function func, boolean isLeft) {
		All all = genExpr(expr.body, func, true);

		Type type = all.type;
		Type returnType = null;
		int off = 0;
		// System.out.println(((StructType) type).tag + ":");
		// System.out.println(expr.attribute);
		if (type instanceof StructType) {
			StructDecl decl = (StructDecl) env.getType(((StructType) type).tag);
			Address addr = all.addr;
			if (decl != null) {
				for (VarDecl element : decl.fields) {
					if (element.name.equals(expr.attribute)) {
						returnType = element.type;
						break;
					} else {
						off += fix(getSize(element.type));
						// System.out.println(element.name + " " +
						// getSize(element.type));
					}
				}
				// System.out.println(off);
			}
			Temp tmp = new Temp();
			func.vars.add(new BasicVariable(tmp, 4, false));
			if (!isLeft
					&& (returnType instanceof IntType
							|| returnType instanceof CharType || returnType instanceof PointerType)) {
				func.body.add(new ArrayRead(tmp, addr, new IntegerConst(off),
						getSize(returnType)));
				return new All(returnType, tmp);
			}
			if (off > 0) {
				func.body.add(new ArithmeticExpr(tmp, addr, ArithmeticOp.ADD,
						new IntegerConst(off)));
				return new All(returnType, tmp);
			} else
				return new All(returnType, addr);
		} else {
			UnionDecl decl = (UnionDecl) env.getType(((UnionType) type).tag);
			Address addr = all.addr;
			if (decl != null) {
				for (VarDecl element : decl.fields) {
					if (element.name.equals(expr.attribute)) {
						returnType = element.type;
						break;
					}
				}
			}
			Temp tmp = new Temp();
			func.vars.add(new BasicVariable(tmp, 4, false));
			if (!isLeft
					&& (returnType instanceof IntType
							|| returnType instanceof CharType || returnType instanceof PointerType)) {
				func.body.add(new ArrayRead(tmp, addr, new IntegerConst(off),
						getSize(returnType)));
				return new All(returnType, tmp);
			}
			if (off > 0) {
				func.body.add(new ArithmeticExpr(tmp, addr, ArithmeticOp.ADD,
						new IntegerConst(off)));
				return new All(returnType, tmp);
			} else
				return new All(returnType, addr);
		}
	}

	private All genSelfIncrement(SelfIncrement expr, Function func) {
		All all = genExpr(expr.body, func, false);
		Temp tmp = new Temp();
		func.vars.add(new BasicVariable(tmp, 4, false));
		func.body.add(new Assign(tmp, all.addr));
		BinaryExpr be = new BinaryExpr(expr.body, BinaryOp.ADD, new IntConst(1));
		be = new BinaryExpr(expr.body, BinaryOp.ASSIGN, be);
		genBinaryExpr(be, func);
		return new All(all.type, tmp);
	}

	private All genSelfDecrement(SelfDecrement expr, Function func) {
		All all = genExpr(expr.body, func, false);
		Temp tmp = new Temp();
		func.vars.add(new BasicVariable(tmp, 4, false));
		func.body.add(new Assign(tmp, all.addr));
		BinaryExpr be = new BinaryExpr(expr.body, BinaryOp.SUB, new IntConst(1));
		be = new BinaryExpr(expr.body, BinaryOp.ASSIGN, be);
		genBinaryExpr(be, func);
		return new All(all.type, tmp);
	}

	private All genArrayAccess(ArrayAccess expr, Function func, boolean isLeft) {
		All body = genExpr(expr.body, func, false);
		All subscript = genExpr(expr.subscript, func, false);

		Type tb = body.type;

		Type returnType = null;
		if (tb instanceof ArrayType)
			returnType = ((ArrayType) tb).baseType;
		else if (tb instanceof PointerType)
			returnType = ((PointerType) tb).baseType;
		/*
		 * if (returnType instanceof ArrayType) {
		 * System.out.println(((ArrayType) returnType).curSize); }
		 */
		Temp off = new Temp();
		func.vars.add(new BasicVariable(off, 4, false));
		func.body.add(new ArithmeticExpr(off, subscript.addr, ArithmeticOp.MUL,
				new IntegerConst(getSize(returnType))));
		func.body
				.add(new ArithmeticExpr(off, body.addr, ArithmeticOp.ADD, off));

		if (isLeft) {
			return new All(returnType, off);
		} else {
			if (returnType instanceof StructType
					|| returnType instanceof UnionType
					|| returnType instanceof ArrayType) {
				return new All(returnType, off);
			} else {
				Temp t = new Temp();
				func.vars.add(new BasicVariable(t, 4, false));
				func.body.add(new MemoryRead(t, off, getSize(returnType)));
				return new All(returnType, t);
			}
		}
	}

	private Param makeParam(All all, Function func) {
		if (all.type instanceof ArrayType) {
			if (all.addr instanceof Name) {
				Temp tmp = new Temp();
				func.vars.add(new BasicVariable(tmp, 4, false));
				func.body.add(new AddressOf(tmp, (Name) all.addr));
				return new BasicParam(tmp, true);
			} else {
				return new BasicParam(all.addr, true);
			}
		} else if (all.type instanceof StructType
				|| all.type instanceof UnionType) {
			int len = getSize(all.type);
			return new ArrayParam(all.addr, ZERO, len);
		} else {
			return new BasicParam(all.addr, false);
		}
	}

	private All genFunctionCall(FunctionCall expr, Function func) {
		Object obj = env.getIden(expr.body);
		FunctionDecl decl = (FunctionDecl) obj;
		List<Param> list = new LinkedList<Param>();
		for (int i = 0; i < expr.args.size(); ++i) {
			All all = genExpr(expr.args.get(i), func, false);
			list.add(makeParam(all, func));
		}
		for (int i = 0; i < expr.args.size(); ++i) {
			func.body.add(list.get(i));
		}
		if (decl.name.toString().equals("printf")) {
			func.body
					.add(new Call(null, decl.name.toString(), expr.args.size()));
			return new All(decl.returnType, null);
		}
		Temp t = new Temp();
		All ret = new All(decl.returnType, t);
		Param pa;
		if (ret.type instanceof StructType || ret.type instanceof UnionType) {
			func.vars.add(new ArrayVariable(t, getSize(ret.type), false));
			pa = makeParam(ret, func);
		} else {
			func.vars.add(new BasicVariable(t, 4, false));
			pa = makeParam(ret, func);
		}
		func.body.add(new Call(pa, decl.name.toString(), expr.args.size()));
		if (pa instanceof BasicParam) {
			return new All(decl.returnType, ((BasicParam) pa).src);
		} else {
			Temp tmp = new Temp();
			func.vars.add(new BasicVariable(tmp, 4, false));
			func.body.add(new AddressOf(tmp, ((ArrayParam) pa).name));
			return new All(decl.returnType, tmp);
		}
	}

	private All genIdentifier(Identifier expr, Function func, boolean isLeft) {
		Object t = env.getIden(expr.symbol);
		Name addr = (Name) env.getAddress(expr.symbol);
		if (isLeft || t instanceof ArrayType || t instanceof StructType
				|| t instanceof UnionType) {
			Temp tmp = new Temp();
			func.vars.add(new BasicVariable(tmp, 4, false));
			func.body.add(new AddressOf(tmp, addr));
			return new All((Type) t, tmp);
		}
		return new All((Type) t, env.getAddress(expr.symbol));
	}

	private All genIntConst(IntConst expr) {
		return new All(new IntType(), new IntegerConst(expr.value));
	}

	private All genCharConst(CharConst expr) {
		int len = expr.value.length();
		if (len == 3)
			return new All(new CharType(), new IntegerConst(
					expr.value.charAt(1)));
		else {
			if (expr.value.charAt(2) == 'n') {
				return new All(new CharType(), new IntegerConst(10));
			} else
				return new All(new CharType(), new IntegerConst(0));
		}
	}

	private All genStringConst(StringConst expr, Function func) {
		int len = expr.value.length() - 1;
		if (opt.get(Symbol.get(expr.value)) != null) {
			return new All(new ArrayType(new CharType(), new IntConst(len)),
					opt.get(Symbol.get(expr.value)));
		}
		__start.body.add(makeParam(new All(new IntType(),
				new IntegerConst(len * 4)), func));
		Temp t = new Temp();
		__start.vars.add(new BasicVariable(t, 4, false));
		__start.body.add(new Call(new BasicParam(t, true), "malloc", 1));
		for (int i = 0; i < len - 1; ++i) {
			__start.body.add(new ArrayWrite(t, new IntegerConst(i * 4),
					new IntegerConst(expr.value.charAt(i + 1)), 4));
		}
		__start.body.add(new ArrayWrite(t, new IntegerConst((len - 1) * 4),
				new IntegerConst(0), 4));
		opt.put(Symbol.get(expr.value), t);
		return new All(new ArrayType(new CharType(), new IntConst(len)), t);
	}
}
