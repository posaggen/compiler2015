package compiler2015.translate;

import compiler2015.ast.Symbol;
import compiler2015.ir.*;

import java.io.*;
import java.util.*;

public class CodeGenerator {

	private java.util.Dictionary<Address, Object> dict = new java.util.Hashtable<Address, Object>();
	private java.util.Dictionary<Address, Integer> getSize = new java.util.Hashtable<Address, Integer>();
	private java.util.Dictionary<Address, Boolean> isSpecial = new java.util.Hashtable<Address, Boolean>();
	private java.util.Dictionary<Address, Integer> count = new java.util.Hashtable<Address, Integer>();
	private java.util.Dictionary<Address, Boolean> erase = new java.util.Hashtable<Address, Boolean>();
	int[] st = new int[100000];
	IR _ir;
	int top = 0;
	int offset = 0;

	public void add(Address addr) {
		if (!(addr instanceof Temp) && !(addr instanceof Name)) return;
		int t = count.get(addr);
		count.put(addr, t + 1);
	}

	public void generateCount(IR ir) {
		for (Function func : ir.fragments) {
			for (Variable element : func.args) {
				count.put(element.addr, 0);
			}
			for (Variable element : func.vars) {
				count.put(element.addr, 0);
			}
		}
		for (Function func : ir.fragments) {
			for (Quadruple element : func.body) {
				if (element instanceof Assign) {
					add(((Assign) element).dest);
					add(((Assign) element).src);
				} else if (element instanceof ArithmeticExpr) {
					add(((ArithmeticExpr) element).dest);
					add(((ArithmeticExpr) element).src1);
					add(((ArithmeticExpr) element).src2);
				} else if (element instanceof ArrayRead) {
					add(((ArrayRead) element).dest);
					add(((ArrayRead) element).offset);
					add(((ArrayRead) element).src);
				} else if (element instanceof ArrayWrite) {
					add(((ArrayWrite) element).dest);
					add(((ArrayWrite) element).offset);
					add(((ArrayWrite) element).src);
				} else if (element instanceof MemoryRead) {
					add(((MemoryRead) element).dest);
					add(((MemoryRead) element).src);
				} else if (element instanceof MemoryWrite) {
					add(((MemoryWrite) element).dest);
					add(((MemoryWrite) element).src);
				} else if (element instanceof AddressOf) {
					add(((AddressOf) element).dest);
					add(((AddressOf) element).src);
				} else if (element instanceof IfFalseGoto) {
					add(((IfFalseGoto) element).src1);
					add(((IfFalseGoto) element).src2);
				} else if (element instanceof BasicParam) {
					add(((BasicParam) element).src);
				} else if (element instanceof ArrayParam) {
					add(((ArrayParam) element).name);
				}
			}
		}
	}

	public void PeepholeOptimization(IR ir) {
		generateCount(ir);
		for (Function func : ir.fragments) {
			boolean flag = false;
			Address addr = null;
			Address last = null;
			List<Quadruple> body = new LinkedList<Quadruple>();
			for (Quadruple element : func.body) {
				body.add(element);
				if (element instanceof AddressOf) {
					if (count.get(((AddressOf) element).dest) == 2) {
						flag = true;
						addr = ((AddressOf) element).dest;
						last = ((AddressOf) element).src;
						continue;
					}
				}
				if (flag && (element instanceof MemoryWrite)) {
					if (((MemoryWrite) element).dest == addr) {
						body.remove(body.size() - 1);
						body.remove(body.size() - 1);
						body.add(new Assign(last, ((MemoryWrite) element).src));
						erase.put(addr, true);
					}
				}
				addr = null;
				last = null;
				flag = false;
			}
			func.body = body;
		}
		for (Function func : ir.fragments) {
			List<Variable> vars = new LinkedList<Variable>();
			for (Variable element : func.vars) {
				if (erase.get(element.addr) != null) 
					continue;
				vars.add(element);
			}
			func.vars = vars;
		}
	}

	public void generate(IR ir) {
		PeepholeOptimization(ir);
//		ir.show();
		_ir = ir;
		for (Function element : ir.fragments) {
			if (element.name.equals("__start"))
				generateFunction(element);
		}
		for (Function element : ir.fragments) {
			if (!element.name.equals("__start"))
				generateFunction(element);
		}
		printPrintf();
	}

	public void printPrintf() {

		System.out.println("printf_loop:");
		System.out.println("lw $a0, 0($a2)");
		System.out.println("beq $a0, 0, printf_end");
		System.out.println("add $a2, $a2, 4");
		System.out.println("beq $a0, 92, printf_slash");
		System.out.println("beq $a0, '%', printf_fmt");
		System.out.println("li $v0, 11");
		System.out.println("syscall");
		System.out.println("b printf_loop");
		System.out.println("printf_end:");
		System.out.println("jr $ra");

		System.out.println("printf_fmt:");
		System.out.println("lw $a0, 0($a2)");
		System.out.println("add $a2, $a2, 4");
		System.out.println("beq $a0, 'd', printf_int");
		System.out.println("beq $a0, 's', printf_str");
		System.out.println("beq $a0, 'c', printf_char");
		System.out.println("beq $a0, '%', printf_char");
		System.out.println("beq $a0, '.', printf_width");
		System.out.println("beq $a0, '0', printf_width");
		System.out.println("b printf_loop");
		System.out.println("printf_int:");
		System.out.println("lw $a0, 0($a1)");
		System.out.println("sub $a1, $a1, 4");
		System.out.println("li $v0, 1");
		System.out.println("syscall");
		System.out.println("b printf_loop");

		System.out.println("printf_str:");
		System.out.println("lw $a3, 0($a1)");
		System.out.println("sub $a1, $a1, 4");
		System.out.println("printf_str_lb1:");
		System.out.println("lw $a0, 0($a3)");
		System.out.println("add $a3, $a3, 4");
		System.out.println("beq $a0, 0, printf_str_lb2");
		System.out.println("li $v0, 11");
		System.out.println("syscall");
		System.out.println("b printf_str_lb1");
		System.out.println("printf_str_lb2:");
		System.out.println("b printf_loop");

		System.out.println("printf_char:");
		System.out.println("lw $a0, 0($a1)");
		System.out.println("sub $a1, $a1, 4");
		System.out.println("li $v0, 11");
		System.out.println("syscall");
		System.out.println("b printf_loop");

		System.out.println("printf_width:");
		System.out.println("lw $t0, 0($a2)");
		System.out.println("add $a2, $a2, 8");
		System.out.println("lw $a3, 0($a1)");
		System.out.println("sub $a1, $a1, 4");
		System.out.println("li $a0, 48");
		System.out.println("li $v0, 11");
		System.out.println("beq $t0, '3', print_width_lb2");
		System.out.println("bgt $a3, 999, print_width_lb1");
		System.out.println("syscall");
		System.out.println("print_width_lb2:");
		System.out.println("bgt $a3, 99, print_width_lb1");
		System.out.println("syscall");
		System.out.println("bgt $a3, 9, print_width_lb1");
		System.out.println("syscall");
		System.out.println("print_width_lb1:");
		System.out.println("move $a0, $a3");
		System.out.println("li $v0, 1");
		System.out.println("syscall");
		System.out.println("b printf_loop");

		System.out.println("printf_slash:");
		System.out.println("lw $a0, 0($a2)");
		System.out.println("add $a2, $a2, 4");
		System.out.println("beq $a0, 'n', printf_enter");
		System.out.println("beq $a0, 't', printf_tab");
		System.out.println("b printf_loop");

		System.out.println("printf_enter:");
		System.out.println("li $a0, 10");
		System.out.println("li $v0, 11");
		System.out.println("syscall");
		System.out.println("b printf_loop");

		System.out.println("printf_tab:");
		System.out.println("li $a0, 9");
		System.out.println("li $v0, 11");
		System.out.println("syscall");
		System.out.println("b printf_loop");
	}

	public void lw(String tmp, Address addr) {
		if (addr instanceof IntegerConst) {
			System.out
					.println("li " + tmp + ", " + ((IntegerConst) addr).value);
			return;
		}
		Object obj = dict.get(addr);
		if (obj instanceof String) {
			System.out.println("lw " + tmp + ", " + (String) obj);
		} else {
			System.out.println("lw " + tmp + ", " + (int) obj + "($sp)");
		}
	}

	public void sw(String tmp, Address addr) {
		Object obj = dict.get(addr);
		if (obj instanceof String) {
			System.out.println("sw " + tmp + ", " + (String) obj);
		} else {
			System.out.println("sw " + tmp + ", " + (int) obj + "($sp)");
		}
	}

	public void generateAssign(Assign quad) {
		lw("$t0", quad.src);
		sw("$t0", quad.dest);
	}

	public void generateMemoryRead(MemoryRead quad) {
		load("$t0", quad.src);
		System.out.println("lw $t0, 0($t0)");
		sw("$t0", quad.dest);
	}

	public void generateMemoryWrite(MemoryWrite quad) {
		Object obj = dict.get(quad.dest);
		if (obj instanceof String) {
			System.out.println("lw " + "$t0, " + (String) obj);
		} else {
			System.out.println("lw $t0, " + (int) obj + "($sp)");
		}
		lw("$t1", quad.src);
		System.out.println("sw $t1, 0($t0)");
	}

	public void save(String tmp, Address addr) {
		sw(tmp, addr);
	}

	public void generateAddressOf(AddressOf quad, boolean isSpecial) {
		Object obj = dict.get(quad.src);
		if (obj instanceof String) {
			System.out.println("la $t0, " + (String) obj);
		} else {
			if (!isSpecial)
				System.out.println("add $t0,$sp," + (int) obj);
			else {
				System.out.println("lw $t0, " + (int) obj + "($sp)");
			}
		}
		save("$t0", quad.dest);
	}

	public void generateLabel(Label quad) {
		System.out.println("___label" + quad.num + ":");
	}

	public void generateGoto(Goto quad) {
		System.out.println("j ___label" + quad.label.num);
	}

	public void load(String tmp, Address addr) {
		lw(tmp, addr);
	}

	public void generateIfTrueGoto(IfTrueGoto quad) {
		load("$t0", quad.src1);
		load("$t1", quad.src2);
		if (quad.op == RelationalOp.EQ) {
			System.out.println("beq $t0,$t1,___label" + quad.label.num);
		} else if (quad.op == RelationalOp.GE) {
			System.out.println("bge $t0,$t1,___label" + quad.label.num);
		} else if (quad.op == RelationalOp.GT) {
			System.out.println("bgt $t0,$t1,___label" + quad.label.num);
		} else if (quad.op == RelationalOp.LE) {
			System.out.println("ble $t0,$t1,___label" + quad.label.num);
		} else if (quad.op == RelationalOp.LT) {
			System.out.println("blt $t0,$t1,___label" + quad.label.num);
		} else if (quad.op == RelationalOp.NE) {
			System.out.println("bne $t0,$t1,___label" + quad.label.num);
		}
	}

	public void generateArithmeticExpr(ArithmeticExpr quad) {
		if (quad.op == ArithmeticOp.ADD) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("add $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.AND) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("and $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.DIV) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("div $t0, $t1");
			System.out.println("mflo $t0");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.MINUS) {
			load("$t0", quad.src1);
			System.out.println("neg $t0, $t0");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.MOD) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("div $t0, $t1");
			System.out.println("mfhi $t0");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.MUL) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("mul $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.OR) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("or $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.SHL) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("sllv $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.SHR) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("srlv $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.SUB) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("sub $t0, $t0, $t1");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.TILDE) {
			load("$t0", quad.src1);
			System.out.println("not $t0, $t0");
			save("$t0", quad.dest);
		} else if (quad.op == ArithmeticOp.XOR) {
			load("$t0", quad.src1);
			load("$t1", quad.src2);
			System.out.println("xor $t0, $t0, $t1");
			save("$t0", quad.dest);
		}
	}

	public void generateBasicParam(BasicParam quad) {
		System.out.println("sub $t0, $sp," + (offset + 4));
		if (quad.src instanceof Temp || quad.src instanceof Name) {
			load("$t1", quad.src);
			System.out.println("sw $t1, 0($t0)");
		} else if (quad.src instanceof IntegerConst) {
			System.out.println("li $t1, " + ((IntegerConst) quad.src).value);
			System.out.println("sw $t1, 0($t0)");
		}
		offset += 4;
		++top;
		st[top] = 4;
	}

	public void generateArrayParam(ArrayParam quad) {
		// System.out.println("!");
		System.out.println("sub $t0, $sp," + (offset + quad.size));
		Object obj = dict.get(quad.name);
		if (obj instanceof String) {
			System.out.println("lw $t1, " + (String) obj);
		} else {
			System.out.println("lw $t1, " + (int) obj + "($sp)");
		}
		for (int i = 0; i < quad.size; i += 4) {
			System.out.println("lw $t2, " + i + "($t1)");
			System.out.println("sw $t2, " + i + "($t0)");
		}
		offset += quad.size;
		++top;
		st[top] = quad.size;
		// System.out.println("!");
	}

	public void generatePrintf(Call quad) {
		int n = quad.numOfParams;
		int delta = 0;
		for (int i = top; i > top - n + 1; --i) {
			delta += st[i];
		}
		System.out.println("lw $a2, " + (-offset + delta) + "($sp)");
		System.out.println("sub $a1, $sp, " + (offset - delta + 4));
		System.out.println("sw $ra, " + (-offset - 4) + "($sp)");
		System.out.println("jal printf_loop");
		System.out.println("lw $ra, " + (-offset - 4) + "($sp)");
		top -= n;
		offset -= delta + 4;
	}

	public void generateGetChar(Call quad) {
		System.out.println("li $v0, 12");
		System.out.println("syscall");
		save("$v0", ((BasicParam) quad.returnValue).src);
	}

	public void generateMalloc(Call quad) {
		System.out.println("li $v0, 9");
		System.out.println("lw $a0, " + (-offset) + "($sp)");
		--top;
		offset -= 4;
		System.out.println("syscall");
		save("$v0", ((BasicParam) quad.returnValue).src);
	}

	int varsSize(String str) {
		for (Function func : _ir.fragments)
			if (func.name.equals(str)) {
				int varsize = 0;
				for (Variable element : func.vars) {
					varsize += fix(element.size);
				}
				return varsize;
			}
		return 0;
	}

	public void generateCall(Call quad) {
		if (quad.callee.equals("printf")) {
			generatePrintf(quad);
			return;
		} else if (quad.callee.equals("getchar")) {
			generateGetChar(quad);
			return;
		} else if (quad.callee.equals("malloc")) {
			generateMalloc(quad);
			return;
		}
		int sz = 0;
		for (int i = 0; i < quad.numOfParams; ++i) {
			sz += st[top - i];
		}
		top -= quad.numOfParams;
		sz += 4;
		offset += 4;
		int tmp = offset;

		// save $ra

		System.out.println("sub $sp, $sp," + offset);
		System.out.println("sw $ra, 0($sp)");
		offset = 0;

		// call

		System.out.println("jal __" + quad.callee);
		int vsz = varsSize(quad.callee);

		// get returnValue

		if (quad.returnValue instanceof BasicParam) {
			Object obj = dict.get(((BasicParam) quad.returnValue).src);
			if (obj instanceof String) {
				System.out.println("la $t1, " + (String) obj);
			} else {
				System.out.println("add $t1, $sp, " + (vsz + tmp + (int) obj));
			}
			System.out.println("sw $v0, 0($t1)");
		} else {
			ArrayParam ap = (ArrayParam) quad.returnValue;
			Object obj = dict.get(ap.name);
			if (obj instanceof String) {
				System.out.println("la $t1, " + (String) obj);
			} else {
				System.out.println("add $t1, $sp, " + (vsz + tmp + (int) obj));
			}
			for (int i = 0; i < ap.size; i += 4) {
				System.out.println("lw $t0, " + i + "($v0)");
				System.out.println("sw $t0, " + i + "($t1)");
			}
		}
		System.out.println("add $sp, $sp, " + vsz);

		// get old $ra & $sp

		offset = tmp;
		System.out.println("lw $ra, 0($sp)");
		System.out.println("add $sp, $sp," + offset);
		offset = tmp;
		offset -= sz;
	}

	public void getAddress(String tmp, Address addr) {
		Object obj = dict.get(addr);
		if (obj instanceof String) {
			System.out.println("la " + tmp + ", " + (String) obj);
		} else {
			if (!isSpecial.get(addr))
				System.out.println("add " + tmp + ", $sp, " + (int) obj);
			else
				System.out.println("lw " + tmp + ", " + (int) obj + "($sp)");
		}
	}

	public void generateArrayRead(ArrayRead quad) {
		load("$t0", quad.src);
		System.out.println("lw $t0, " + ((IntegerConst) quad.offset).value
				+ "($t0)");
		getAddress("$t1", quad.dest);
		if (quad.size == 4) {
			System.out.println("sw $t0, 0($t1)");
		} else {
			System.out.println("sb $t0, 0($t1)");
		}
	}

	public void generateArrayWrite(ArrayWrite quad) {
		if (quad.src instanceof IntegerConst) {
			System.out.println("li $t0, " + ((IntegerConst) quad.src).value);
		} else {
			load("$t0", quad.src);
			System.out.println("lw $t0, 0($t0)");
		}
		System.out.println("lw $t1, " + getName(quad.dest));
		if (quad.size == 4) {
			System.out.println("sw $t0, " + ((IntegerConst) quad.offset).value
					+ "($t1)");
		} else {
			System.out.println("sb $t0, " + ((IntegerConst) quad.offset).value
					+ "($t1)");
		}
	}

	public void generateReturn(Return quad) {
		if (quad.value instanceof BasicParam) {
			BasicParam pa = (BasicParam) quad.value;
			load("$v0", pa.src);
		} else if (quad.value instanceof ArrayParam) {
			ArrayParam pa = (ArrayParam) quad.value;
			load("$v0", pa.name);
		}
		System.out.println("jr $ra");
	}

	public void generateQuadruple(Quadruple quad) {
		if (quad instanceof Assign) {
			generateAssign((Assign) quad);
		} else if (quad instanceof ArrayRead) {
			generateArrayRead((ArrayRead) quad);
		} else if (quad instanceof ArrayWrite) {
			generateArrayWrite((ArrayWrite) quad);
		} else if (quad instanceof ArithmeticExpr) {
			generateArithmeticExpr((ArithmeticExpr) quad);
		} else if (quad instanceof MemoryRead) {
			generateMemoryRead((MemoryRead) quad);
		} else if (quad instanceof MemoryWrite) {
			generateMemoryWrite((MemoryWrite) quad);
		} else if (quad instanceof AddressOf) {
			generateAddressOf((AddressOf) quad,
					isSpecial.get(((AddressOf) quad).src));
		} else if (quad instanceof Label) {
			generateLabel((Label) quad);
		} else if (quad instanceof Goto) {
			generateGoto((Goto) quad);
		} else if (quad instanceof IfTrueGoto) {
			generateIfTrueGoto((IfTrueGoto) quad);
		} else if (quad instanceof BasicParam) {
			generateBasicParam((BasicParam) quad);
		} else if (quad instanceof ArrayParam) {
			generateArrayParam((ArrayParam) quad);
		} else if (quad instanceof Call) {
			generateCall((Call) quad);
		} else if (quad instanceof Return) {
			generateReturn((Return) quad);
		}
	}

	String getName(Address addr) {
		if (addr instanceof Name) {
			return "_" + ((Name) addr).name;
		} else {
			return "__t" + ((Temp) addr).num;
		}
	}

	public void generateFunction(Function func) {
		if (func.name.equals("__start")) {
			System.out.println(".data");
			for (Variable element : func.vars) {
				dict.put(element.addr, getName(element.addr));
				getSize.put(element.addr, element.size);
				isSpecial.put(element.addr, false);
				System.out.println(getName(element.addr) + ": .space "
						+ fix(element.size));
			}
			System.out.println(".text");
			System.out.println("_start:");
			for (Quadruple element : func.body) {
				generateQuadruple(element);
			}
			System.out.println("jr $ra");
		} else {
			if (func.name.equals("main")) {
				System.out.println("main:");
				System.out.println("sub $sp, $sp, 4");
				System.out.println("sw $ra, 0($sp)");
				System.out.println("jal _start");
				System.out.println("lw $ra, 0($sp)");
				System.out.println("add $sp, $sp, 4");
			} else {
				System.out.println("__" + func.name + ":");
			}
			int varsize = 0;
			for (Variable element : func.vars) {
				varsize += fix(element.size);
			}
			System.out.println("sub $sp, $sp, " + varsize);
			int tmp = 0;
			for (int i = func.vars.size() - 1; i >= 0; --i) {
				Address addr = func.vars.get(i).addr;
				int sz = func.vars.get(i).size;
				dict.put(addr, tmp);
				getSize.put(addr, sz);
				isSpecial.put(addr, false);
				tmp += fix(sz);
			}
			tmp += 4;
			for (int i = func.args.size() - 1; i >= 0; --i) {
				Address addr = func.args.get(i).addr;
				int sz = func.args.get(i).size;
				dict.put(addr, tmp);
				getSize.put(addr, sz);
				isSpecial.put(addr, func.args.get(i).isSpecial);
				tmp += fix(sz);
			}
			for (Quadruple element : func.body) {
				generateQuadruple(element);
			}
			System.out.println("jr $ra");
		}
	}

	int fix(int x) {
		return x + (4 - x % 4) % 4;
	}
}
