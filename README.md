# Compiler 2015 #
## A compiler for C language implemented by JAVA ##
1. Abstract Syntax Tree (AST) construction
2. Semantic check
3. Intermediate Representation construction
4. MIPS code generation
5. Optimization